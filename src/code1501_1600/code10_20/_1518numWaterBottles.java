package code1501_1600.code10_20;

/**
 * 小区便利店正在促销，用 numExchange 个空酒瓶可以兑换一瓶新酒。你购入了 numBottles 瓶酒。
 *
 * 如果喝掉了酒瓶中的酒，那么酒瓶就会变成空的。
 *
 * 请你计算 最多 能喝到多少瓶酒
 *
 * 输入：numBottles = 9, numExchange = 3
 * 输出：13
 * 解释：你可以用 3 个空酒瓶兑换 1 瓶酒。
 * 所以最多能喝到 9 + 3 + 1 = 13 瓶酒。
 *
 * 输入：numBottles = 15, numExchange = 4
 * 输出：19
 * 解释：你可以用 4 个空酒瓶兑换 1 瓶酒。
 * 所以最多能喝到 15 + 3 + 1 = 19 瓶酒。
 *
 * 输入：numBottles = 5, numExchange = 5
 * 输出：6
 *
 * 输入：numBottles = 2, numExchange = 3
 * 输出：2
 */
public class _1518numWaterBottles {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 35.2 MB     * , 在所有 Java 提交中击败了     * 48.36%     * 的用户
     * @param numBottles
     * @param numExchange
     * @return
     */
    public static int numWaterBottles(int numBottles, int numExchange) {
        int nullBottles = numBottles;
        int result = numBottles;
        int elseNullBottles = 0;
        while (nullBottles+elseNullBottles>=numExchange){
            nullBottles += elseNullBottles;
            elseNullBottles = nullBottles%numExchange;
            nullBottles = nullBottles/numExchange;
            result += nullBottles;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(numWaterBottles(15,8));
    }

    /**
     * 题解归纳数学方法
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 35.1 MB     * , 在所有 Java 提交中击败了     * 77.18%     * 的用户
     * @param numBottles
     * @param numExchange
     * @return
     */
    public int numWaterBottles1(int numBottles, int numExchange) {
        return numBottles >= numExchange ? (numBottles - numExchange) / (numExchange - 1) + 1 + numBottles : numBottles;
    }

}
