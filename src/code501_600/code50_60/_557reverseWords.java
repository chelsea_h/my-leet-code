package code501_600.code50_60;

/**
 * 给定一个字符串，你需要反转字符串中每个单词的字符顺序，同时仍保留空格和单词的初始顺序。
 * 输入："Let's take LeetCode contest"
 * 输出："s'teL ekat edoCteeL tsetnoc"
 */
public class _557reverseWords {

    /**
     * 执行用时：     * 5 ms     * , 在所有 Java 提交中击败了     * 69.40%     * 的用户
     * 内存消耗：     * 39.2 MB     * , 在所有 Java 提交中击败了     * 21.78%     * 的用户
     * @param s
     * @return
     */
    public String reverseWords(String s) {
        StringBuffer result = new StringBuffer();
        String[] words = s.split(" ");
        for (int i = 0; i < words.length; i++) {
            result.append(reverseWord(words[i]));
            result.append(" ");
        }
        return result.substring(0,s.length());
    }

    public String reverseWord(String s){
        char[] word = s.toCharArray();
        char temp = ' ';
        for (int i = 0; i < s.length() / 2; i++) {
            temp = s.charAt(i);
            word[i] = word[s.length()-i-1];
            word[s.length()-i-1] = temp;
        }
        return new String(word);
    }

}
