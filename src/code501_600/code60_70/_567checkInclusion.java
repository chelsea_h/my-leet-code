package code501_600.code60_70;

/**
 * 给你两个字符串 s1 和 s2 ，写一个函数来判断 s2 是否包含 s1 的排列。如果是，返回 true ；否则，返回 false 。
 *
 * 换句话说，s1 的排列之一是 s2 的 子串 。
 *
 * 输入：s1 = "ab" s2 = "eidbaooo"
 * 输出：true
 * 解释：s2 包含 s1 的排列之一 ("ba").
 *
 * 输入：s1= "ab" s2 = "eidboaoo"
 * 输出：false
 */
public class _567checkInclusion {

    /**
     * 思考，字符数组查找即可。
     *
     * 以为只是简单的判断字符个数是否相等而已，结果没想到题意是还需要顺序一致。此方法就不行了
     * @param s1
     * @param s2
     * @return
     */
    public static boolean checkInclusion0(String s1, String s2) {
        if(s1.equals(s2))return true;
        int[] s2Num = new int[26];
        // 首先记录字母x对应的频数
        for (char a : s2.toCharArray()) {
            s2Num[a-'a']++;
        }
        // 遍历s1，每个字母都有则返回true，反之返回false
        for (char a : s1.toCharArray()) {
            if(s2Num[a-'a']!=0){
                s2Num[a-'a']--;
            }else {
                return false;
            }
        }
        return true;
    }

    /**
     * 首先爆搜解决了，但效率极低。考虑其他。这次应该是KMP算法了
     *
     * 优化：每次滑动窗口时，只统计了一进一出两个字符，但却比较了整个数组。
     * 从这个角度出发，可以使用变量diff记录cnt1和cnt2不同值的个数。这样问题就转换成了判断diff是否等于0
     * 每次滑动窗口，一进一出的字符记为x和y
     * 若x=y 则对cnt无影响，直接跳过。
     * 若x！= y，修改cnt2之前若cnt1=cnt2，则diff加一。修改后若等于则diff减一。
     *
     * 执行用时：     * 287 ms     * , 在所有 Java 提交中击败了     * 8.99%     * 的用户
     * 内存消耗：     * 38.9 MB     * , 在所有 Java 提交中击败了     * 88.43%     * 的用户
     * @param s1
     * @param s2
     * @return
     */
    public static boolean checkInclusion(String s1, String s2) {
        if(s1.equals(s2))return true;
        if(s1.length()>s2.length())return false;
        boolean result = false;
        for (int i = 0; i <= s2.length() - s1.length(); i++) {
            if(checkInclusion0(s1,s2.substring(i,i+s1.length()))){
                result = true;
            }
        }
        return result;
    }

    /**
     * 第二种方法判断是否是子字符串，也错了，原题意是判断是否包含子字符串的排列，子问题就是上述写的方法！！！。读个题目太累人了
     * @param s1
     * @param s2
     * @return
     */
    public static boolean checkInclusion1(String s1, String s2) {
        if(s1.equals(s2))return true;
        for (int i = 0; i <= s2.length() - s1.length(); i++) {
            for (int j = 0; j < s1.length(); j++) {
                if(s1.charAt(j)==s2.charAt(i+j)){
                    if(j==s1.length()-1){
                        return true;
                    }
                }else {
                    break;
                }
            }
        }

        for (int i = 0; i <= s2.length() - s1.length(); i++) {
            for (int j = s1.length()-1; j >= 0; j--) {
                if(s1.charAt(j)==s2.charAt(i+s1.length()-j-1)){
                    if(j==0){
                        return true;
                    }
                }else {
                    break;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(checkInclusion("ab","eidbaooo"));
    }

}
