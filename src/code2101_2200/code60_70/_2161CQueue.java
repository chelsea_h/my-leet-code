package code2101_2200.code60_70;

import java.util.Stack;

/**
 * 用两个栈实现一个队列。队列的声明如下，请实现它的两个函数 appendTail 和 deleteHead ，
 * 分别完成在队列尾部插入整数和在队列头部删除整数的功能。(若队列中没有元素，deleteHead 操作返回 -1 )
 *
 * 输入：
 * ["CQueue","appendTail","deleteHead","deleteHead"]
 * [[],[3],[],[]]
 * 输出：[null,null,3,-1]
 *
 * 输入：
 * ["CQueue","deleteHead","appendTail","appendTail","deleteHead","deleteHead"]
 * [[],[],[5],[2],[],[]]
 * 输出：[null,-1,null,null,5,2]
 *
 */
public class _2161CQueue {

    Stack<Integer> stack1;
    Stack<Integer> stack2;

    public _2161CQueue() {
        stack1 = new Stack<>();
        stack2 = new Stack<>();
    }

    /**
     * 执行用时：     * 45 ms     * , 在所有 Java 提交中击败了     * 39.73%     * 的用户
     * 内存消耗：     * 46.5 MB     * , 在所有 Java 提交中击败了     * 90.03%     * 的用户
     * @param value
     */
    public void appendTail(int value) {
        stack1.push(value);
    }

    public int deleteHead() {
        if(stack2.isEmpty()){
            while (!stack1.isEmpty()){
                stack2.push(stack1.pop());
            }
        }
        if(stack2.isEmpty()){
            return-1;
        }else {
            return stack2.pop();
        }
    }

}
