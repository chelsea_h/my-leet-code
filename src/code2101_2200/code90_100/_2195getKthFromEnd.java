package code2101_2200.code90_100;

/**
 * 输入一个链表，输出该链表中倒数第k个节点。为了符合大多数人的习惯，本题从1开始计数，即链表的尾节点是倒数第1个节点。
 *
 * 例如，一个链表有 6 个节点，从头节点开始，它们的值依次是 1、2、3、4、5、6。这个链表的倒数第 3 个节点是值为 4 的节点。
 *
 * 给定一个链表: 1->2->3->4->5, 和 k = 2.
 *
 * 返回链表 4->5.
 */
public class _2195getKthFromEnd {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.1 MB     * , 在所有 Java 提交中击败了     * 83.20%     * 的用户
     * @param head
     * @param k
     * @return
     */
    public ListNode getKthFromEnd(ListNode head, int k) {
        ListNode result = head;
        ListNode right = head;
        for (int i = 0; i < k; i++) {
            if(right==null)return null;
            right = right.next;
        }
        while (right!=null){
            result = result.next;
            right = right.next;
        }
        return result;
    }

}
