package code2101_2200.code90_100;

/**
 * 输入两个递增排序的链表，合并这两个链表并使新链表中的节点仍然是递增排序的。
 *
 * 输入：1->2->4, 1->3->4
 * 输出：1->1->2->3->4->4
 */
public class _2197mergeTwoLists {

    /**
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 38.01%     * 的用户
     * 内存消耗：     * 38.7 MB     * , 在所有 Java 提交中击败了     * 70.55%     * 的用户
     * @param l1
     * @param l2
     * @return
     */
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode head = new ListNode();
        head.next = null;
        ListNode node = head;
        ListNode temp;
        while (l1!=null&&l2!=null){
            temp = new ListNode();
            if(l1.val>l2.val){
                temp.val = l2.val;
                l2 = l2.next;
            }else {
                temp.val = l1.val;
                l1 = l1.next;
            }
            node.next = temp;
            node = temp;
        }
        if(l1!=null){
            node.next = l1;
        }else {
            node.next = l2;
        }
        return head.next;
    }

    /**
     * 题解算法，时间效率高；
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38.7 MB     * , 在所有 Java 提交中击败了     * 9.65%     * 的用户
     * @param l1
     * @param l2
     * @return
     */
    public ListNode mergeTwoLists1(ListNode l1, ListNode l2) {
        ListNode dum = new ListNode(0), cur = dum;
        while(l1 != null && l2 != null) {
            if(l1.val < l2.val) {
                cur.next = l1;
                l1 = l1.next;
            }
            else {
                cur.next = l2;
                l2 = l2.next;
            }
            cur = cur.next;
        }
        cur.next = l1 != null ? l1 : l2;
        return dum.next;
    }

}
