package code2101_2200.code80_90;

import java.util.Stack;

/**
 * 定义栈的数据结构，请在该类型中实现一个能够得到栈的最小元素的 min 函数在该栈中，调用 min、push 及 pop 的时间复杂度都是 O(1)。
 *
 * MinStack minStack = new MinStack();
 * minStack.push(-2);
 * minStack.push(0);
 * minStack.push(-3);
 * minStack.min();   --> 返回 -3.
 * minStack.pop();
 * minStack.top();      --> 返回 0.
 * minStack.min();   --> 返回 -2.
 *
 */
public class _2183minStack {

    Stack<Integer> stack;
    int min;
    /** initialize your data structure here. */
    public _2183minStack() {
        stack = new Stack<>();
    }

    /**
     * 执行用时：     * 16 ms     * , 在所有 Java 提交中击败了     * 99.98%     * 的用户
     * 内存消耗：     * 40.2 MB     * , 在所有 Java 提交中击败了     * 88.65%     * 的用户
     * @param x
     */
    public void push(int x) {
        if(stack.isEmpty()){
            min = x;
        }else {
            min = min>x?x:min;
        }
        stack.push(x);
    }

    public void pop() {
        int temp = stack.pop();
        if(temp==min&&!stack.isEmpty()){
            min = stack.peek();
            for (Integer t : stack){
                min = min<t?min:t;
            }
        }
    }

    public int top() {
        return stack.peek();
    }

    public int min() {
        return min;
    }

}
