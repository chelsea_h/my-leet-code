package code2101_2200.code80_90;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 从上到下打印出二叉树的每个节点，同一层的节点按照从左到右的顺序打印。
 *
 * 例如:
 * 给定二叉树: [3,9,20,null,null,15,7],
 *
 * [3,9,20,15,7]
 */
public class _2189levelOrder {

    /**
     * 思考 ： 还不是层次遍历？上提好像是一样的
     *
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 98.11%     * 的用户
     * 内存消耗：     * 38.6 MB     * , 在所有 Java 提交中击败了     * 39.16     * 的用户
     * @param root
     * @return
     */
    public int[] levelOrder(TreeNode root) {
        if(root==null)return new int[]{};
        List<Integer> list = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        TreeNode node;
        int size;
        queue.offer(root);
        while (!queue.isEmpty()){
            size = queue.size();
            for (int i = 0; i < size; i++) {
                node = queue.poll();
                list.add(node.val);
                if(node.left!=null) queue.offer(node.left);
                if(node.right!=null) queue.offer(node.right);
            }
        }
        int[] ans = new int[list.size()];
        for (int i = 0; i < ans.length; i++) {
            ans[i] = list.get(i);
        }
        return ans;
    }

}
