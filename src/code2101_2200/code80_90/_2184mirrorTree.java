package code2101_2200.code80_90;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * 请完成一个函数，输入一个二叉树，该函数输出它的镜像。
 *
 * 输入：root = [4,2,7,1,3,6,9]
 * 输出：[4,7,2,9,6,3,1]
 */
public class _2184mirrorTree {

    /**
     * 思考，从最底层开始，让他的左右子树互换。
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 35.9 MB     * , 在所有 Java 提交中击败了     * 92.22%     * 的用户
     *
     * @param root
     * @return
     */
    public TreeNode mirrorTree(TreeNode root) {
        if(root==null||(root.right==null&&root.left==null))return root;
        TreeNode node;
        ArrayDeque<TreeNode> stack = new ArrayDeque<>();
        stack.addFirst(root);
        while (!stack.isEmpty()){
            node = stack.pollFirst();
            if(node==null)continue;
            if(node.left!=null)stack.addFirst(node.left);
            if(node.right!=null)stack.addFirst(node.right);
            if(node.left==null&&node.right==null){
                continue;
            }else {
                TreeNode temp = node.left;
                node.left = node.right;
                node.right = temp;
            }
        }
        return root;
    }

}
