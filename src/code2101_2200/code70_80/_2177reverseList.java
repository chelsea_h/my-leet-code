package code2101_2200.code70_80;

import com.sun.jdi.ThreadReference;

import java.util.List;

/**
 * 定义一个函数，输入一个链表的头节点，反转该链表并输出反转后链表的头节点。
 */
public class _2177reverseList {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 37.8 MB     * , 在所有 Java 提交中击败了     * 92.87%     * 的用户
     * @param head
     * @return
     */
    public ListNode reverseList(ListNode head) {
        if(head==null||head.next==null)return head;
        ListNode result = new ListNode();
        ListNode node;
        while (head!=null){
            node = head;
            head = head.next;
            node.next = result.next;
            result.next = node;
        }
        return result.next;
    }

}
