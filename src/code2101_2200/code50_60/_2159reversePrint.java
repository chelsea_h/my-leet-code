package code2101_2200.code50_60;

/**
 * 输入一个链表的头节点，从尾到头反过来返回每个节点的值（用数组返回）。
 */
public class _2159reversePrint {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38 MB     * , 在所有 Java 提交中击败了     * 99.81%     * 的用户
     * @param head
     * @return
     */
    public int[] reversePrint(ListNode head) {
        if(head==null)return new int[]{};
        if(head.next==null)return new int[]{head.val};
        ListNode node = head;
        int length = 0;
        while (node!=null){
            node = node.next;
            ++length;
        }
        int[] result = new int[length];
        for (int i = result.length-1; i >= 0; i--) {
            result[i] = head.val;
            head = head.next;
        }
        return result;
    }
}
