package code2101_2200.code50_60;

/**
 * 请实现一个函数，把字符串 s 中的每个空格替换成"%20"。
 */
public class _2158replaceSpace {

    /**
     * 方法一：return s.replace(" ","%20");
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.3 MB     * , 在所有 Java 提交中击败了     * 42.47%     * 的用户
     *
     * 方法二：每个字符进行查找
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.3 MB     * , 在所有 Java 提交中击败了     * 55.36%     * 的用户
     * @param s
     * @return
     */
    public String replaceSpace(String s) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i)==' '){
                result.append("%20");
                continue;
            }
            result.append(s.charAt(i));
        }
        return result.toString();
    }

}
