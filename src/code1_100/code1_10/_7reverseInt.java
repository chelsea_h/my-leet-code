package code1_100.code1_10;

/**
 * 给你一个 32 位的有符号整数 x ，返回 x 中每位上的数字反转后的结果。
 *
 * 如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。
 * 假设环境不允许存储 64 位整数（有符号或无符号）。
 *
 * 输入：x = 123
 * 输出：321
 *
 * 解题思路：判断是否溢出的思路：
 * 当出现 ans > MAX_VALUE / 10 且 还有pop需要添加 时，则一定溢出
 * 当出现 ans == MAX_VALUE / 10 且 pop > 7 时，则一定溢出，7是2^31 - 1的个位数
 * 从ans * 10 + pop < MIN_VALUE这个溢出条件来看
 * 当出现 ans < MIN_VALUE / 10 且 还有pop需要添加 时，则一定溢出
 * 当出现 ans == MIN_VALUE / 10 且 pop < -8 时，则一定溢出，8是-2^31的个位数
 *
 */
public class _7reverseInt {

    //min:-2147483648
    //max:2147483647
    public static void main(String[] args) {
        System.out.println(reverse(12));
    }

    //不使用long的最优解，内存消耗相比下面的较大
    public static int reverse1(int x){
        int result = 0;
        while(x != 0) {
            int tmp = result; // 保存计算之前的结果
            result = (result * 10) + (x % 10);
            x /= 10;
            // 将计算之后的结果 / 10，判断是否与计算之前相同，如果不同，证明发生溢出，返回0
            if (result / 10 != tmp) return 0;
        }

        return result;
    }

    //个人认为的最优解
    public static int reverse(int x){
        long n = 0;
        while(x != 0) {
            n = n*10 + x%10;
            x = x/10;
        }
        return (int)n==n? (int)n:0;
    }
    //暴力版本，时间和空间都非常低，上面为优解。
    /*public static int reverse(int x) {
        //判断是否为负数
        int label = 0;
        if (x < 0) {
            label = 1;
        }
        StringBuffer intStr = new StringBuffer();
        intStr.append(x);
        char intChar[] = new char[intStr.length()-label];
        int i;
        for (i = label; i < intStr.length(); i++) {
            intChar[i-label] = intStr.charAt(i);
        }
        //反转字符数组
        char outputChar[] = new char[intStr.length()-label];
        for (i = 0; i < intChar.length; i++) {
            outputChar[i] = intChar[intChar.length-i-1];
        }
        if(outputChar.length==1){
            if(label==0){
                return Integer.parseInt(String.valueOf(outputChar));
            }else {
                String str = "-";
                str = str+outputChar[0];
                return Integer.parseInt(str);
            }
        }
        //将字符数组转为字符串
        StringBuffer outputStr = new StringBuffer();
        if(label==1){
            outputStr.append("-");
        }
        //pop和前n-1位
        int pop = Integer.parseInt(String.valueOf(outputChar[outputChar.length-1]));
        for (int j = 0; j < outputChar.length-1; j++) {
            outputStr.append(outputChar[j]);
        }
        int output = 0;
        output = Integer.parseInt(outputStr.toString());
        if(output>Integer.MAX_VALUE/10||(output==Integer.MAX_VALUE/10&&pop>7)||(output<Integer.MIN_VALUE/10&&pop!=0)||(output==Integer.MIN_VALUE/10&&pop<-8)){
            return 0;
        }
        if(label==1){
            return output*10-pop;
        }else {
            return output*10+pop;
        }
    }*/


    /*//初始算法，一步一步算，但行不通。不得不使用数学算法。
    public static int reverse(int x) {
        //判断是否为负数
        int label = 0;
        if (x < 0) {
            label = 1;
        }
        StringBuffer intStr = new StringBuffer();
        intStr.append(x);
        char intChar[] = new char[intStr.length()-label];
        int i;
        for (i = label; i < intStr.length(); i++) {
            intChar[i-label] = intStr.charAt(i);
        }
        //反转字符数组
        char outputChar[] = new char[intStr.length()-label];
        for (i = 0; i < intChar.length; i++) {
            outputChar[i] = intChar[intChar.length-i-1];
        }

        //将字符数组转为字符串
        StringBuffer outputStr = new StringBuffer();
        if(label==1){
            outputStr.append("-");
        }
        for (int j = 0; j < outputChar.length; j++) {
            outputStr.append(outputChar[j]);
        }

        //将字符串转为最终结果
        int output = 0;
        output = Integer.parseInt(outputStr.toString());
        if(output>=Integer.MAX_VALUE||output<=Integer.MIN_VALUE){
            output = 0;
        }
        return output;
    }*/
}
