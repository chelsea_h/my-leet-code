package code1_100.code1_10;


/**
 * 给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
 *
 * 请你将两个数相加，并以相同形式返回一个表示和的链表。
 *
 * 你可以假设除了数字 0 之外，这两个数都不会以 0 开头
 *
 * 输入：l1 = [2,4,3], l2 = [5,6,4]
 * 输出：[7,0,8]
 * 解释：342 + 465 = 807.
 *
 */
public class _2numberAdd {

    public static void main(String[] args) {
        ListNode l1 = new ListNode(2,new ListNode(4,new ListNode(3)));
        ListNode l2 = new ListNode(5,new ListNode(6,new ListNode(4)));
        ListNode outp = addTwoNumbers(l1, l2);
        while (outp!=null){
            System.out.println(outp.val);
            outp = outp.next;
        }
    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //根节点
        ListNode root = new ListNode(0);
        //临时节点
        ListNode temp = root;
        //标志位判断两数相加是否大于10
        int label = 0;
        while (l1!=null||l2!=null||label!=0){
            int l1Val = l1!=null?l1.val:0;
            int l2Val = l2!=null?l2.val:0;
            int sumVal = l1Val + l2Val + label;
            label = sumVal/10;

            ListNode sumNode = new ListNode(sumVal % 10);
            temp.next = sumNode;
            temp = sumNode;

            if(l1!=null){
                l1 = l1.next;
            }
            if(l2!=null){
                l2 = l2.next;
            }
        }
        return root.next;
    }

}

/**
 * 链表节点结构体
 */
class ListNode{
    int val;
    ListNode next;

    //无参构造函数
    ListNode(){};

    //有参构造函数
    ListNode(int val) {
        this.val = val;
    }

    //有参构造函数
    ListNode(int val, ListNode next) {
        this.val = val; this.next = next;
    }
}