package code1_100.code1_10;

/**
 * 回文数
 * 给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。
 *
 * 回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。例如，121 是回文，而 123 不是。
 *
 */
public class _9isPalindrome {

    public static void main(String[] args) {
        System.out.println(isPalindrome(1221));
    }

    /**
     * 目前最好的方法，精髓在于多加了一个数据前提判断，并且while循环只有二分之一的循环次数。
     * @param x
     * @return
     */
    public static boolean isPalindrome2(int x){
        if (x == 0) return true;
        if (x < 0 || x % 10 == 0) return false;
        int reversed = 0;
        while (x > reversed) {
            reversed = reversed * 10 + x % 10;
            x /= 10;
        }
        return x == reversed || x == reversed / 10;
    }

    /**
     * 此方法时间复杂度和空间复杂度相较稍低，看来字符串操作比取余操作更耗时间和空间
     * @param x
     * @return
     */
    public static boolean isPalindrome1(int x){
        if(x<0)
            return false;
        int rem=0,y=0;
        int quo=x;
        while(quo!=0){
            rem=quo%10;
            y=y*10+rem;
            quo=quo/10;
        }
        return y==x;
    }

    /**
     * 可以通过 但时间复杂度和空间复杂度都较高。但如果是判断回文字符串，此方法可用
     * @param x
     * @return
     */
    public static boolean isPalindrome(int x) {
        if(x<0){
            return false;
        }
        String str = String.valueOf(x);
        for (int i = 0; i < (str.length())/2; i++) {
            if(str.charAt(i)!=str.charAt(str.length()-i-1)){
                return false;
            }
        }
        return true;
    }
}
