package code1_100.code50_60;

/**
 * 给你一个字符串 s，由若干单词组成，单词之间用空格隔开。返回字符串中最后一个单词的长度。如果不存在最后一个单词，请返回 0 。
 *
 * 单词 是指仅由字母组成、不包含任何空格字符的最大子字符串。
 *
 * 输入：s = "Hello World"
 * 输出：5
 *
 * 输入：s = " "
 * 输出：0
 *
 *
 1 <= s.length <= 104
 s 仅有英文字母和空格 ' ' 组成

 */
public class _58lengthOfLastVocu {

    /**
     * 执行用时：1 ms, 在所有 Java 提交中击败了37.87% 的用户
     * 内存消耗：36.8 MB, 在所有 Java 提交中击败了45.30% 的用户
     * @param s
     * @return
     */
    public static int lengthOfLastWord(String s) {
        String[] vocuS = s.split(" ");
        if (vocuS.length==0){
            return 0;
        }
        return vocuS[vocuS.length-1].length();
    }

    /**
     * 第二种方法，从后往前遍历字符串
     * 0 ms, 在所有 Java 提交中击败了100.00% 的用户
     * 内存消耗：36.7 MB, 在所有 Java 提交中击败了61.09% 的用户
     *
     * @param s
     */
    public static int lengthOfLastWord1(String s) {
        int ans = 0;
        boolean isVocu = false;
        for (int i = s.length()-1; i >= 0; i--) {
            if (s.charAt(i)!=' '){
                isVocu = true;
                ans++;
            }
            if (isVocu && s.charAt(i)==' '){
                break;
            }
        }
        return ans;
    }


    /**
     * 第三种方法，势必减去一个变量
     * 执行用时：0 ms, 在所有 Java 提交中击败了100.00% 的用户
     * 内存消耗：36.5 MB, 在所有 Java 提交中击败了80.27% 的用户
     * 看来内存还有提高的可能啊！
     * @param s
     * @return
     */
    public static int lengthOfLastWord2(String s) {
        int ans = 0;
        int i;
        // 先找出最后一个单词（过滤掉最后的空格）
        for (i = s.length()-1; i > 0; i--) {
            if (s.charAt(i)==' '){
                continue;
            }else {
                break;
            }
        }
        // 过滤后计数即可
        while (i>=0&&s.charAt(i)!=' '){
            ans++;
            i--;
        }
        return ans;
    }


    public static void main(String[] args) {
        String str = "   ";
        System.out.println(lengthOfLastWord2(str));
    }

}
