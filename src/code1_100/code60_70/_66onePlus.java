package code1_100.code60_70;

/**
 * 给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。
 *
 * 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
 *
 * 你可以假设除了整数 0 之外，这个整数不会以零开头。
 *
 * 输入：digits = [1,2,3]
 * 输出：[1,2,4]
 * 解释：输入数组表示数字 123。
 *
 * 输入：digits = [4,3,2,1]
 * 输出：[4,3,2,2]
 * 解释：输入数组表示数字 4321。
 *
 * 输入：digits = [0]
 * 输出：[1]
 *
 1 <= digits.length <= 100
 0 <= digits[i] <= 9
 *
 */
public class _66onePlus {


    /**
     * 就按照正常思维来就行
     *
     * 执行用时：0 ms, 在所有 Java 提交中击败了100.00% 的用户
     * 36.9 MB, 在所有 Java 提交中击败了49.93% 的用户
     *
     * @param digits
     * @return
     */
    public static int[] plusOne(int[] digits) {
        for (int i = digits.length-1; i >= 0 ; i--) {
            if (digits[i]!=9){
                digits[i] += 1;
                break;
            }else if(i==0){
                int[] digtsCopy = new int[digits.length+1];
                digtsCopy[0] = 1;
                return digtsCopy;
            }else {
                digits[i] = 0;
            }
        }
        return digits;
    }


    public static void main(String[] args) {
        int[] digits = {4,3,2,2};
        for (int i : plusOne(digits)) {
            System.out.print(i+" ");
        }
    }

}
