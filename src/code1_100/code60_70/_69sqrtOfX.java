package code1_100.code60_70;

/**
 * 实现 int sqrt(int x) 函数。
 *
 * 计算并返回 x 的平方根，其中 x 是非负整数。
 *
 * 由于返回类型是整数，结果只保留整数的部分，小数部分将被舍去。
 *
 * 输入4 输出2  输入8 输出2（舍去了整数）
 *
 */
public class _69sqrtOfX {

    /**
     * 二分查找
     *
     * @param x
     * @return
     */
    public static int mySqrt(int x) {
        if(x==0){
            return 0;
        }
        if(x==1){
            return 1;
        }
        int left = 1;
        int right = 2<<15;
        while (left<right){
            int mid = (left+right) / 2 + 1;
            // 注意：这里为了避免乘法溢出，改用除法
            if (mid > x / mid) {
                // 下一轮搜索区间是 [left..mid - 1]
                right = mid - 1;
            } else {
                // 下一轮搜索区间是 [mid..right]
                left = mid;
            }
        }
        return left;
    }

    public static void main(String[] args) {
        System.out.println(mySqrt(42354));
    }

}
