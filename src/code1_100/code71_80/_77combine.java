package code1_100.code71_80;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。
 *
 * 你可以按 任何顺序 返回答案。
 *
 * 输入：n = 4, k = 2
 * 输出：
 * [
 *   [2,4],
 *   [3,4],
 *   [2,3],
 *   [1,2],
 *   [1,3],
 *   [1,4],
 * ]
 *
 * 输入：n = 1, k = 1
 * 输出：[[1]]
 */
public class _77combine {

    /**
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 99.97%     * 的用户
     * 内存消耗：     * 39.9 MB     * , 在所有 Java 提交中击败了     * 61.17%     * 的用户
     */
    List<List<Integer>> ans = new ArrayList<>();
    List<Integer> temp = new ArrayList<>();
    public List<List<Integer>> combine(int n, int k) {
        dfs(1,n,k);
        return ans;
    }

    public void dfs(int cur,int n,int k){
        //剪枝：temp长度加上区间【cur，n】的长度小于k，不可能构成长度k的temp
        if((temp.size()+(n-cur+1))<k){
            return;
        }
        //记录合法的答案
        if(temp.size()==k){
            ans.add(new ArrayList<>(temp));
            return;
        }
        //考虑选择当前位置
        temp.add(cur);
        dfs(cur+1,n,k);
        temp.remove(temp.size()-1);
        // 考虑不选择当前位置
        dfs(cur+1,n,k);
    }

}
