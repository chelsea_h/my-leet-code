package code1_100.code20_30;

/**
 * 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
 *
 * 不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
 *
 * 元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 *
 * 输入：nums = [3,2,2,3], val = 3
 * 输出：2, nums = [2,2]
 * 解释：函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。你不需要考虑数组中超出新长度后面的元素。例如，函数返回的新长度为 2 ，而 nums = [2,2,3,3] 或 nums = [2,2,0,0]，也会被视作正确答案。
 *
 * 输入：nums = [0,1,2,2,3,0,4,2], val = 2
 * 输出：5, nums = [0,1,4,0,3]
 * 解释：函数应该返回新的长度 5, 并且 nums 中的前五个元素为 0, 1, 3, 0, 4。注意这五个元素可为任意顺序。你不需要考虑数组中超出新长度后面的元素。
 *
 * 另一种解答：复制移除，目测效率更高，但循环次数高。
 * public int removeElement(int[] nums, int val) {
 *         int ans = 0;
 *         for(int num: nums) {
 *             if(num != val) {
 *                 nums[ans] = num;
 *                 ans++;
 *             }
 *         }
 *         return ans;
 *     }
 *
 */
public class _27removeElement {

    public static void main(String[] args) {
        int nums[] = {};
        System.out.println(removeElement(nums,1));
    }

    /**
     * 根据示例，不用考虑顺序，则可从后往前遍历，有相同则和前面不同调换位置
     *
     * 一次通过，内存消耗过大。
     * 执行用时：0 ms, 在所有 Java 提交中击败了100.00% 的用户
     * 内存消耗：37.1 MB, 在所有 Java 提交中击败了39.22% 的用户
     *
     * 注释掉初始无用判断后，内存提升
     * 执行用时：0 ms, 在所有 Java 提交中击败了100.00% 的用户
     * 内存消耗：36.9 MB, 在所有 Java 提交中击败了72.47% 的用户
     *
     * @param nums
     * @param val
     * @return
     */
    public static int removeElement(int[] nums, int val) {

        /*if(nums.length==0){
            return 0;
        }*/

        //由于交换数据，内存效率低，但遍历数组元素少
        int pre = 0,next = nums.length-1;
        int temp;
        while (next>=pre){
            if(nums[pre]==val&&nums[next]!=val){
                temp = nums[pre];
                nums[pre] = nums[next];
                nums[next] = temp;
            }
            if(nums[pre]!=val){
                pre++;
            }
            if(nums[next]==val){
                next--;
            }
        }

        for (int i = 0; i < pre; i++) {
            System.out.println(nums[i]);
        }
        System.out.println("------------------");
        return pre;
    }
}
