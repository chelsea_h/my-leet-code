package code1_100.code20_30;

/**
 * 给定一个排序数组，你需要在 原地 删除重复出现的元素，使得每个元素只出现一次，返回移除后数组的新长度。
 * <p>
 * 不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
 * <p>
 * 给定数组 nums = [1,1,2],
 * <p>
 * 函数应该返回新的长度 2, 并且原数组 nums 的前两个元素被修改为 1, 2。
 * <p>
 * 你不需要考虑数组中超出新长度后面的元素。
 */
public class _26removeDuplicates {

    public static void main(String[] args) {
        int[] intNums = {1,1};
        System.out.println(removeDuplicates(intNums));
    }


    /**
     * 一次提交
     * 执行用时：1 ms, 在所有 Java 提交中击败了80.54% 的用户
     * 内存消耗：39.9 MB, 在所有 Java 提交中击败了97.55% 的用户
     * @param nums
     * @return
     */
    public static int removeDuplicates(int[] nums) {
        if(nums.length==0){
            return 0;
        }
        //设置标志位，表示前n位不同数字
        int label = 0;
        int temp = nums[0];
        //O（n）时间复杂度
        for (int i = 1; i < nums.length; i++) {
            if(temp != nums[i]){
                label++;
                temp = nums[i];
            }
            nums[label] = nums[i];
        }

        for (int i = 0; i < label+1; i++) {
            System.out.println(nums[i]);
        }
        System.out.println("------------------");
        return label+1;
    }
}
