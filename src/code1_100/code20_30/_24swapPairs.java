package code1_100.code20_30;

/**
 * 给你一个链表，两两交换其中相邻的节点，并返回交换后链表的头节点。
 * 你必须在不修改节点内部的值的情况下完成本题（即，只能进行节点交换）。
 *
 * 输入：head = [1,2,3,4]
 * 输出：[2,1,4,3]
 *
 * 输入：head = []
 * 输出：[]
 *
 * 输入：head = [1]
 * 输出：[1]
 */
public class _24swapPairs {

    /**
     * 直接两两换的话会导致断行，忽然想到递归可能更方便？
     * @param head
     * @return
     */
    public ListNode swapPairs0(ListNode head) {
        if(head==null||head.next==null)return head;
        ListNode result = head.next;
        // 左右后节点
        ListNode left = head;
        ListNode right = result;
        ListNode last = null;
        while (right.next!=null){
            // 更新last节点
            last = right.next;
            // 左右节点更换
            left.next = last;
            right.next = left;
            // 更新左右节点
            left = left.next;
            right = last.next;
        }
        return result;
    }

    /**
     * 题解中的递归算法
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.3 MB     * , 在所有 Java 提交中击败了     *43.04%     * 的用
     *
     * @param head
     * @return
     */
    public ListNode swapPairs(ListNode head) {
        if(head==null||head.next==null)return head;
        ListNode newHead = head.next;
        head.next = swapPairs(newHead.next);
        newHead.next = head;
        return newHead;
    }

    /**
     * 题解方法2，迭代
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.2 MB     * , 在所有 Java 提交中击败了     * 10.34%     * 的用户
     * @param head
     * @return
     */
    public ListNode swapPairs1(ListNode head) {
        ListNode dummyHead = new ListNode(0);
        dummyHead.next = head;
        ListNode temp = dummyHead;
        while (temp.next!=null&&temp.next.next!=null){
            ListNode node1 = temp.next;
            ListNode node2 = temp.next.next;
            temp.next = node2;
            node1.next = node2.next;
            node2.next = node1;
            temp = node1;
        }
        return dummyHead.next;
    }

}
