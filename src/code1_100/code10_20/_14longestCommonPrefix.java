package code1_100.code10_20;

/**
 * 编写一个函数来查找字符串数组中的最长公共前缀。
 *
 * 如果不存在公共前缀，返回空字符串 ""
 *
 * 输入：strs = ["flower","flow","flight"]
 * 输出："fl"
 */
public class _14longestCommonPrefix {

    public static void main(String[] args) {
        String[] strs = {"flower","flow","flight"};
        System.out.println(longestCommonPrefix1(strs));
    }

    /**
     * 执行用时：1 ms, 在所有 Java 提交中击败了84.09% 的用户
     * 内存消耗：36.4 MB, 在所有 Java 提交中击败了82.22% 的用户
     * @param strs
     * @return
     */
    public static String longestCommonPrefix1(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        String prefix = strs[0];
        int count = strs.length;
        for (int i = 1; i < count; i++) {
            prefix = longestCommonPrefix(prefix, strs[i]);
            if (prefix.length() == 0) {
                break;
            }
        }
        return prefix;
    }

    /**
     * 寻找两个字符串的最长公共前缀子串
     * @param str1
     * @param str2
     * @return
     */
    public static String longestCommonPrefix(String str1, String str2) {
        int length = Math.min(str1.length(), str2.length());
        int index = 0;
        while (index < length && str1.charAt(index) == str2.charAt(index)) {
            index++;
        }
        return str1.substring(0, index);
    }

    /**
     * 寻找最长公共前缀，数据预处理耗费了很多的执行次数
     * 此方法每次找一个字符，然后遍历整个数组进行匹配，效率低。
     * 第二种方法：最长公共前缀是：第一和第二个字符串的前缀和第三个字符继续查找，以此类推。
     *
     * 执行用时：2 ms, 在所有 Java 提交中击败了30.54% 的用户
     * 内存消耗：36.8 MB, 在所有 Java 提交中击败了19.41% 的用户
     * @param strss
     * @return
     */
    public static String longestCommonPrefix(String[] strss) {
        //预处理数据
        if(strss.length==0){
            return "";
        }
        if(strss.length==1){
            return strss[0];
        }
        int lowStr = lowStr(strss);
        if(lowStr==0){
            return "";
        }
        StringBuffer longest = new StringBuffer("");
        boolean isEqual = false;
        char temp;
        for (int i = 0; i < lowStr; i++) {
            temp = strss[0].charAt(i);
            for (int j = 0; j < strss.length; j++) {
                if(temp!=strss[j].charAt(i)){
                    isEqual = false;
                    break;
                }else {
                    isEqual = true;
                }
            }
            if(isEqual==true){
                longest.append(temp);
            }else {
                break;
            }
        }
        return String.valueOf(longest);
    }

    /**
     * 寻找字符串数组中字符串最少的字符长度
     * @param strs
     * @return
     */
    public static int lowStr(String[] strs){
        int output = strs[0].length();
        for (int i = 1; i < strs.length; i++) {
            if(output>strs[i].length()){
                output = strs[i].length();
            }
        }
        return output;
    }
}
