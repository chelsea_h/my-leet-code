package code1_100.code30_40;

import java.util.HashMap;

/**
 *给定一个正整数 n ，输出外观数列的第 n 项。
 *
 * 「外观数列」是一个整数序列，从数字 1 开始，序列中的每一项都是对前一项的描述。
 *
 * 你可以将其视作是由递归公式定义的数字字符串序列：
 *
 *     countAndSay(1) = "1"
 *     countAndSay(n) 是对 countAndSay(n-1) 的描述，然后转换成另一个数字字符串。
 *
 * 前五项如下：
 * 1.     1
 * 2.     11
 * 3.     21
 * 4.     1211
 * 5.     111221
 * 第一项是数字 1
 * 描述前一项，这个数是 1 即 “ 一 个 1 ”，记作 "11"
 * 描述前一项，这个数是 11 即 “ 二 个 1 ” ，记作 "21"
 * 描述前一项，这个数是 21 即 “ 一 个 2 + 一 个 1 ” ，记作 "1211"
 * 描述前一项，这个数是 1211 即 “ 一 个 1 + 一 个 2 + 二 个 1 ” ，记作
 *
 */
public class _38countAndSay {

    public static void main(String[] args) {
        System.out.println(splitStr("aaavbb"));
    }

    /**
     *
     * @param n
     * @return
     */
    public static String countAndSay(int n){
        String output = "";
        String temp = "";
        String start = "1";
        //第一层循环
        for (int i = 0; i < n; i++) {


        }

        return output;
    }

    /**
     * 第一次想的方法，想着先分开再重新填入，写了这个分开方法。
     * 局限性：map会打乱填入顺序，并且key值唯一，会造成数据丢失
     * 并且不需要这样，可以直接遍历字符串进行修改
     * @param str
     * @return
     */
    public static HashMap<Integer,Character> splitStr(String str){
        HashMap<Integer, Character> hashMap = new HashMap<>();
        int index = 0;
        char label = str.charAt(0);
        for (int i = 0; i < str.length(); i++) {
            if(label==str.charAt(i)){
                index++;
            }else {
                hashMap.put(index,label);
                index=1;
                label = str.charAt(i);
            }
        }
        hashMap.put(index,label);
        return hashMap;
    }
}




















