package code1_100.code30_40;

/**
 * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
 *
 * 你可以假设数组中无重复元素
 *
 * 输入: [1,3,5,6], 5
 * 输出: 2
 *
 * 输入: [1,3,5,6], 0
 * 输出: 0
 *
 */
public class _35searchInsert {

    public static void main(String[] args) {
        int nums[] = {1,3};
        System.out.println(searchInsert(nums,2));
    }

    /**
     * 由于是有序数组，二分查找相对友好
     *
     * 执行用时：0 ms, 在所有 Java 提交中击败了100.00% 的用户
     * 内存消耗：38.2 MB, 在所有 Java 提交中击败了45.56% 的用户
     *
     * @param nums
     * @param target
     * @return
     */
    public static int searchInsert(int[] nums, int target) {
        int len = nums.length;
        if (len == 0) {
            return 0;
        }

        int left = 0;
        // 因为有可能数组的最后一个元素的位置的下一个是我们要找的，故右边界是 len
        int right = len;
        while (left < right) {
            int mid = left + (right - left) / 2;
            // 小于 target 的元素一定不是解
            if (nums[mid] < target) {
                // 下一轮搜索的区间是 [mid + 1, right]
                left = mid + 1;
            } else {
                // 下一轮搜索的区间是 [left, mid]
                right = mid;
            }
        }
        return left;
    }


    /**
     * 第二次写，一次通过！
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38.3 MB     * , 在所有 Java 提交中击败了     * 5.29%     * 的用户
     * @param nums
     * @param target
     * @return
     */
    public static int searchInsert1(int[] nums,int target){
        //初始化数据，将边界条件先排除
        if(nums[0]>=target)return 0;
        if(nums[nums.length-1]<target)return nums.length;
        int left = 0;
        int right = nums.length-1;
        int mid;
        while (left<right){
            // 防止left+right导致int数据溢出
            mid = left+(right-left)/2;
            if(nums[mid]==target){
                return mid;
            }else if(nums[mid]<target){
                left = mid + 1;
            }else {
                right = mid;
            }
        }
        return right;
    }
}
