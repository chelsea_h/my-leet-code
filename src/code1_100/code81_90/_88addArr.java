package code1_100.code81_90;

/**
 * 给你两个按 非递减顺序 排列的整数数组 nums1 和 nums2，另有两个整数 m 和 n ，分别表示 nums1 和 nums2 中的元素数目。
 *
 * 请你 合并 nums2 到 nums1 中，使合并后的数组同样按 非递减顺序 排列。
 *
 * 注意：最终，合并后数组不应由函数返回，而是存储在数组 nums1 中。为了应对这种情况，nums1 的初始长度为 m + n，
 * 其中前 m 个元素表示应合并的元素，后 n 个元素为 0 ，应忽略。nums2 的长度为 n 。
 *
 * 输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
 * 输出：[1,2,2,3,5,6]
 * 解释：需要合并 [1,2,3] 和 [2,5,6] 。
 * 合并结果是 [1,2,2,3,5,6] ，其中斜体加粗标注的为 nums1 中的元素。
 *
 */
public class _88addArr {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38.4 MB     * , 在所有 Java 提交中击败了     * 73.79%     * 的用户
     * @param nums1
     * @param m
     * @param nums2
     * @param n
     */
    /*public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int label1 = m - 1;
        int label2 = n - 1;
        if(m==0){
            for (int i = 0; i < n; i++) {
                nums1[i] = nums2[i];
            }
        }
        if(n==0){
            return;
        }

        for (int i = 0; i < nums1.length; i++) {
            if(label1<0&&label2>=0){
                for (int j = 0; j < label2+1; j++) {
                    nums1[j] = nums2[j];
                }
                break;
            }
            if(label2<0){
                break;
            }

            if(nums2[label2]>nums1[label1]){
                nums1[label1+label2+1] = nums2[label2];
                label2--;
            }else {
                nums1[label1+label2+1] = nums1[label1];
                label1--;
            }
        }
        for (int i = 0; i < nums1.length; i++) {
            System.out.print(nums1[i]+" ");
        }
    }*/

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38.4 MB     * , 在所有 Java 提交中击败了     * 69.95%     * 的用户
     * @param nums1
     * @param m
     * @param nums2
     * @param n
     */
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = nums1.length;
        while (n > 0) {
            if (m > 0 && nums1[m-1] > nums2[n-1]) {
                nums1[--i] = nums1[--m];
                //替代swap，参考官方题解“逆向双指针解法”的公式
            }else{
                nums1[--i] = nums2[--n];
                //替代swap，参考官方题解“逆向双指针解法”的公式
            }

        }
    }

    public static int max(int a,int b){
        return a>b?a:b;
    }

    public static void main(String[] args) {
        int[] nums1 = {2,0};
        int[] nums2 = {1};
        merge(nums1,1,nums2,1);
    }

}
