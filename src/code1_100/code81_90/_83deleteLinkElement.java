package code1_100.code81_90;

/**
 * 83 删除排序链表中的元素
 * 存在一个按升序排列的链表，给你这个链表的头节点 head ，请你删除所有重复的元素，使每个元素 只出现一次 。
 *
 * 返回同样按升序排列的结果链表。链表定义已给。
 *
 */
public class _83deleteLinkElement {

    /**
     * 执行用时：0 ms, 在所有 Java 提交中击败了100.00% 的用户
     * 内存消耗：37.6 MB, 在所有 Java 提交中击败了93.99% 的用户
     *
     * @param head
     * @return
     */
    public static ListNode deleteDuplicates(ListNode head) {
        if(head==null||head.next==null){
            return head;
        }
        ListNode cur = head;
        ListNode next = head.next;
        while (next!=null){
            if(cur.val == next.val){
                cur.next = next.next;
                next = next.next;
            }else{
                cur = cur.next;
                next = next.next;
            }
        }
        return head;
    }

    public static void main(String[] args) {
        /**
         * 头插法
         */
        ListNode head = new ListNode();
        head.val = 100;
        for (int i = 0; i < 10; i++) {
            ListNode temp = new ListNode();
            temp.val = 100;
            temp.next = head;
            head = temp;
        }
        printLink(head);
        System.out.println(deleteDuplicates(head));
        printLink(head);
    }

    /**
     * 打印链表
     * @param node
     */
    public static void printLink(ListNode node){
        while (node!=null){
            System.out.print(node.val+" ");
            node = node.next;
        }
    }

}

