package code1_100.code91_100;

/**
 * 给你单链表的头指针 head 和两个整数 left 和 right ，其中 left <= right 。请你反转从位置 left 到位置 right 的链表节点，返回 反转后的链表 。
 *
 * 输入：head = [1,2,3,4,5], left = 2, right = 4
 * 输出：[1,4,3,2,5]
 *
 * 输入：head = [5], left = 1, right = 1
 * 输出：[5]
 */
public class _92reverseBetween {

    /**
     * 自己写的模拟法，对应题解中的第一种方法，暂时还有点问题
     * @param head
     * @param left
     * @param right
     * @return
     */
    public static ListNode reverseBetween(ListNode head, int left, int right) {
        // 最左边的节点
        ListNode start = head;
        // 最右边的节点
        ListNode end = null;
        // 需要反转的首节点和末节点个数。
        ListNode leftNode;
        int rightNode = 0;
        // 反转后的首节点
        ListNode reLeftNode = null;
        ListNode node = head;
        while (node.val!=left){
            node = node.next;
            start = start.next;
        }
        start.next = null;
        leftNode = node;
        while (node!=null&&node.val!=right){
            node = node.next;
            ++rightNode;
        }
        end = node.next;
        // 准备反转中间这些节点，头插法直接遍历即可
        for (int i = 0; i < rightNode; i++) {
            ListNode nowNode = leftNode;
            leftNode = leftNode.next;
            nowNode.next = reLeftNode;
            reLeftNode = nowNode;
        }
        // 链表重组
        start.next = reLeftNode;
        node = start;
        while (node.next!=null){
            node = node.next;
        }
        node.next = end;
        return start;
    }

    public static void main(String[] args) {
        ListNode node = new ListNode(1,new ListNode(2,new ListNode(3,new ListNode(4,new ListNode(5)))));
        reverseBetween(node,2,4);
    }

    /**
     * 题解中的模拟法。
     * @param head
     * @param left
     * @param right
     * @return
     */
    public ListNode reverseBetween1(ListNode head, int left, int right) {
        // 因为头节点有可能发生变化，使用虚拟头节点可以避免复杂的分类讨论
        ListNode dummyNode = new ListNode(-1);
        dummyNode.next = head;

        ListNode pre = dummyNode;
        // 第 1 步：从虚拟头节点走 left - 1 步，来到 left 节点的前一个节点
        // 建议写在 for 循环里，语义清晰
        for (int i = 0; i < left - 1; i++) {
            pre = pre.next;
        }

        // 第 2 步：从 pre 再走 right - left + 1 步，来到 right 节点
        ListNode rightNode = pre;
        for (int i = 0; i < right - left + 1; i++) {
            rightNode = rightNode.next;
        }

        // 第 3 步：切断出一个子链表（截取链表）
        ListNode leftNode = pre.next;
        ListNode curr = rightNode.next;

        // 注意：切断链接
        pre.next = null;
        rightNode.next = null;

        // 第 4 步：同第 206 题，反转链表的子区间
        reverseLinkedList(leftNode);

        // 第 5 步：接回到原来的链表中
        pre.next = rightNode;
        leftNode.next = curr;
        return dummyNode.next;
    }

    private void reverseLinkedList(ListNode head) {
        // 也可以使用递归反转一个链表
        ListNode pre = null;
        ListNode cur = head;

        while (cur != null) {
            ListNode next = cur.next;
            cur.next = pre;
            pre = cur;
            cur = next;
        }
    }

    /**
     * 题解中的简便方法。只需要便利一次。
     * 每次将反转链表中的节点头插法插到前面，直到right处
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.2 MB     * , 在所有 Java 提交中击败了     * 7.42%     * 的用户
     * @param head
     * @param left
     * @param right
     * @return
     */
    public ListNode reverseBetween2(ListNode head, int left, int right) {
        // 设置 dummyNode 是这一类问题的一般做法
        ListNode dummyNode = new ListNode(-1);
        dummyNode.next = head;
        ListNode pre = dummyNode;
        for (int i = 0; i < left - 1; i++) {
            pre = pre.next;
        }
        ListNode cur = pre.next;
        ListNode next;
        for (int i = 0; i < right - left; i++) {
            next = cur.next;
            cur.next = next.next;
            next.next = pre.next;
            pre.next = next;
        }
        return dummyNode.next;
    }

}
