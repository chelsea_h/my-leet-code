package code1_100.code40_50;

import java.util.*;

/**
 * 给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。
 *
 * 输入：nums = [1,2,3]
 * 输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
 *
 * 输入：nums = [0,1]
 * 输出：[[0,1],[1,0]]
 *
 * 输入：nums = [1]
 * 输出：[[1]]
 */
public class _46permute {

    /**
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 82.81%     * 的用户
     * 内存消耗：     * 38.7 MB     * , 在所有 Java 提交中击败了     * 47.48%     * 的用户
     * @param nums
     * @return
     */
    public List<List<Integer>> permute(int[] nums) {
        int len = nums.length;
        List<List<Integer>> result = new ArrayList<>();
        if(len==0)return result;

        boolean[] used = new boolean[len];
        Deque<Integer> path = new ArrayDeque<>();
        dfs(nums,len,0,path,used,result);

        return result;
    }
    public void dfs(int[] nums,int len,int depth,Deque<Integer> path,boolean[] used,List<List<Integer>> result){
        if(depth == len){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = 0; i < len; i++) {
            if(used[i]){
                continue;
            }
            path.addLast(nums[i]);
            used[i] = true;
            dfs(nums,len,depth+1,path,used,result);
            path.removeLast();
            used[i] = false;
        }
    }
}
