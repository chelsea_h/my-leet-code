package code1101_1200.code80_90;

/**
 * 给你一个日期，请你设计一个算法来判断它是对应一周中的哪一天。
 *
 * 输入为三个整数：day、month 和 year，分别表示日、月、年。
 *
 * 您返回的结果必须是这几个值中的一个 {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}。
 *
 * 输入：day = 31, month = 8, year = 2019
 * 输出："Saturday"
 *
 * 输入：day = 18, month = 7, year = 1999
 * 输出："Sunday"
 *
 * 输入：day = 15, month = 8, year = 1993
 * 输出："Sunday"
 */
public class _1185dayOfTheWeek {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 35.5 MB     * , 在所有 Java 提交中击败了     * 71.19%     * 的用户
     * @param day
     * @param month
     * @param year
     * @return
     */
    public static String dayOfTheWeek(int day, int month, int year) {
        String[] week = {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
        int[] mouthDays = {31,28,31,30,31,30,31,31,30,31,30,31};
        int allDays = (year-1970)*365;
        // 对闰年单独+1
        for (int i = 1970; i < year; i++) {
            if(i%400==0||(i%100!=0&&i%4==0))++allDays;
        }
        // 如果今年是闰年，二月+1；
        if(year%400==0||(year%100!=0&&year%4==0))++mouthDays[1];

        for (int i = 0; i < month-1; i++) {
            allDays += mouthDays[i];
        }
        allDays += day;
        // 所有天数计算出来后，对7取余返回对应数组中的值就行
        return week[(allDays-4)%7];
    }

    public static void main(String[] args) {
        System.out.println(dayOfTheWeek(4,1,1970));
    }

}
