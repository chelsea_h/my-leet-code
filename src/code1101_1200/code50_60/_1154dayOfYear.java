package code1101_1200.code50_60;

/**
 * 给你一个字符串 date ，按 YYYY-MM-DD 格式表示一个 现行公元纪年法 日期。请你计算并返回该日期是当年的第几天。
 *
 * 通常情况下，我们认为 1 月 1 日是每年的第 1 天，1 月 2 日是每年的第 2 天，依此类推。
 * 每个月的天数与现行公元纪年法（格里高利历）一致。
 *
 * 输入：date = "2019-01-09"
 * 输出：9
 *
 * 输入：date = "2019-02-10"
 * 输出：41
 *
 * 输入：date = "2003-03-01"
 * 输出：60
 *
 * 输入：date = "2004-03-01"
 * 输出：61
 */
public class _1154dayOfYear {

    /**
     * 自己写的
     * 执行用时：9 ms, 在所有 Java 提交中击败了60.61% 的用户
     * 内存消耗：38.9 MB, 在所有 Java 提交中击败了21.18% 的用户
     *
     * 修改之后：
     * 执行用时：7 ms, 在所有 Java 提交中击败了95.13% 的用户
     * 内存消耗：39.1 MB, 在所有 Java 提交中击败了34.71% 的用户
     * @param date
     * @return
     */
    public int dayOfYear(String date) {
        int year = Integer.parseInt(date.substring(0,4));
        int[] mouthNum = {31,28,31,30,31,30,31,31,30,31,30,31};
        if((year%400==0)||(year%4==0&&year%100!=0))++mouthNum[1];
        int ans = 0;
        for (int i = Integer.parseInt(date.substring(5,7))-1; i >= 0 ; i--) {
            ans+=mouthNum[i];
        }
        return ans+Integer.parseInt(date.substring(8,10));
    }

    /**
     * 官方答案
     * 执行用时：7 ms, 在所有 Java 提交中击败了95.13% 的用户
     * 内存消耗：38.9 MB, 在所有 Java 提交中击败了30.43% 的用户
     * @param date
     * @return
     */
    public int dayOfYear1(String date) {
        int year = Integer.parseInt(date.substring(0, 4));
        int month = Integer.parseInt(date.substring(5, 7));
        int day = Integer.parseInt(date.substring(8));

        int[] amount = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
            ++amount[1];
        }

        int ans = 0;
        for (int i = 0; i < month - 1; ++i) {
            ans += amount[i];
        }
        return ans + day;
    }

}
