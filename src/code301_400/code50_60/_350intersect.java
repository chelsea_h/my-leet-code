package code301_400.code50_60;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;

/**
 * 给你两个整数数组 nums1 和 nums2 ，请你以数组形式返回两数组的交集。返回结果中每个元素出现的次数，
 * 应与元素在两个数组中都出现的次数一致（如果出现次数不一致，则考虑取较小值）。可以不考虑输出结果的顺序。
 *
 * 输入：nums1 = [1,2,2,1], nums2 = [2,2]
 * 输出：[2,2]
 *
 * 输入：nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * 输出：[4,9]
 */
public class _350intersect {

    /**
     * 题目看不懂，暂时就按照寻找交集来判断。
     * 方法1：hash表
     * 方法2：排序后双指针
     * 先测试一下hash表
     *
     * 执行用时：     * 3 ms     * , 在所有 Java 提交中击败了     * 42.86%     * 的用户
     * 内存消耗：     * 38.4 MB     * , 在所有 Java 提交中击败了     * 80.54%     * 的用户
     *
     * @param nums1
     * @param nums2
     * @return
     */
    public int[] intersect0(int[] nums1, int[] nums2) {
        HashMap<Integer,Integer> hash = new HashMap<>();
        //首先构建第一个hash表
        for (int i = 0; i < nums1.length; i++) {
            if(hash.keySet().contains(nums1[i])){
                hash.put(nums1[i],hash.get(nums1[i])+1);
            }else {
                hash.put(nums1[i],1);
            }
        }
        int[] result = new int[nums1.length];
        int k = 0;
        // 遍历第二个，然后寻找结果
        for (int i = 0; i < nums2.length; i++) {
            if(hash.keySet().contains(nums2[i])&&hash.get(nums2[i])>0){
                result[k] = nums2[i];
                hash.put(nums2[i],hash.get(nums2[i])-1);
                k++;
            }
        }
        return Arrays.copyOfRange(result,0,k);
    }

    /**
     * 考虑双指针题解：双指针题解 一定得先排序，暂时未掌握堆排序等方法，就先简单的冒泡排序了
     *
     * 执行用时：     * 13 ms     * , 在所有 Java 提交中击败了     * 5.33%     * 的用户
     * 内存消耗：     * 38.4 MB     * , 在所有 Java 提交中击败了     * 81.01%     * 的用户
     *
     * @param nums1
     * @param nums2
     * @return
     */
    public int[] intersect(int[] nums1, int[] nums2) {
        //先对两数组进行排序
        nums1 = sort(nums1);
        nums2 = sort(nums2);
        int[] result = new int[nums1.length];
        // 两个数组的指针
        int k = 0;
        int l = 0;
        int resultPos = 0;
        while (k!= nums1.length&&l!=nums2.length){
            if(nums1[k]==nums2[l]){
                result[resultPos] = nums1[k];
                resultPos++;
                k++;l++;
            }else if(nums1[k]>nums2[l]){
                l++;
            }else {
                k++;
            }
        }
        return Arrays.copyOfRange(result,0,resultPos);
    }

    /**
     * 对数组进行升序排列
     * @param array
     * @return
     */
    public int[] sort(int[] array){
        int temp = array[0];
        for (int i = 0; i < array.length; i++) {
            for (int j = i+1; j < array.length; j++) {
                if(array[i]>array[j]){
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }

}
