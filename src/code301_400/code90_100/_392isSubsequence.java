package code301_400.code90_100;

/**
 * 给定字符串 s 和 t ，判断 s 是否为 t 的子序列。
 *
 * 字符串的一个子序列是原始字符串删除一些（也可以不删除）字符而不改变剩余字符相对位置形成的新字符串。（例如，"ace"是"abcde"的一个子序列，而"aec"不是）。
 *
 * 进阶：
 *
 * 如果有大量输入的 S，称作 S1, S2, ... , Sk 其中 k >= 10亿，你需要依次检查它们是否为 T 的子序列。在这种情况下，你会怎样改变代码？
 *
 * 输入：s = "abc", t = "ahbgdc"
 * 输出：true
 *
 * 输入：s = "axc", t = "ahbgdc"
 * 输出：false
 */
public class _392isSubsequence {

    /**
     * 题解算法
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 85.87%     * 的用户
     * 内存消耗：     * 36.2 MB     * , 在所有 Java 提交中击败了     * 79.40%     * 的用户
     * @param s
     * @param t
     * @return
     */
    public boolean isSubsequence0(String s, String t) {
        int n = s.length(), m = t.length();
        int i = 0, j = 0;
        while (i < n && j < m) {
            if (s.charAt(i) == t.charAt(j)) {
                i++;
            }
            j++;
        }
        return i == n;
    }

    /**
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 85.87%     * 的用户
     * 内存消耗：     * 36.3 MB     * , 在所有 Java 提交中击败了     * 61.26%     * 的用户
     * @param s
     * @param t
     * @return
     */
    public boolean isSubsequence(String s, String t) {
        int sLength = s.length();
        int tLength = t.length();
        if(sLength>tLength)return false;
        if(sLength==tLength&&!s.equals(t))return false;
        int sStart = 0;
        int tStart = 0;
        while (sStart<sLength&&tStart<tLength){
            if(s.charAt(sStart)==t.charAt(tStart)){
                ++sStart;
            }
            ++tStart;
        }
        return sStart==sLength?true:false;
    }

}
