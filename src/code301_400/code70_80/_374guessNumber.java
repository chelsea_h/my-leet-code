package code301_400.code70_80;

/**
 * 猜数字游戏的规则如下：
 *
 *     每轮游戏，我都会从 1 到 n 随机选择一个数字。 请你猜选出的是哪个数字。
 *     如果你猜错了，我会告诉你，你猜测的数字比我选出的数字是大了还是小了。
 *
 * 你可以通过调用一个预先定义好的接口 int guess(int num) 来获取猜测结果，返回值一共有 3 种可能的情况（-1，1 或 0）：
 *
 *     -1：我选出的数字比你猜的数字小 pick < num
 *     1：我选出的数字比你猜的数字大 pick > num
 *     0：我选出的数字和你猜的数字一样。恭喜！你猜对了！pick == num
 *
 * 返回我选出的数字。
 *
 * 输入：n = 10, pick = 6
 * 输出：6
 *
 * 输入：n = 1, pick = 1
 * 输出：1
 *
 * 输入：n = 2, pick = 1
 * 输出：1
 *
 * 输入：n = 2, pick = 2
 * 输出：2
 */
public class _374guessNumber {

    /**
     * 一个简单的二分查找
     *
     * 执行用时：0 ms, 在所有 Java 提交中击败了100.00% 的用户
     * 内存消耗：35.1 MB, 在所有 Java 提交中击败了59.00% 的用户
     * @param n
     * @return
     */
    public int guessNumber(int n) {
        int left = 0;
        int right = n;
        int mid;
        while (left<right){
            mid = left + (right-left) / 2;
            if(guess(mid)==1){
                left = mid + 1;
            }else if(guess(mid)==0){
                return mid;
            }else {
                right = mid;
            }
        }
        return left;
    }

    /**
     * 题目中已实现的后台方法，此处放置只防报错
     * @param mid
     * @return
     */
    private int guess(int mid) {
        return 0;
    }

}
