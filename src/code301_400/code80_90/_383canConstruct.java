package code301_400.code80_90;

import java.util.HashMap;
import java.util.Map;

/**
 * 为了不在赎金信中暴露字迹，从杂志上搜索各个需要的字母，组成单词来表达意思。
 *
 * 给你一个赎金信 (ransomNote) 字符串和一个杂志(magazine)字符串，判断 ransomNote 能不能由 magazines 里面的字符构成。
 *
 * 如果可以构成，返回 true ；否则返回 false 。
 *
 * magazine 中的每个字符只能在 ransomNote 中使用一次。
 *
 * 为了不在赎金信中暴露字迹，从杂志上搜索各个需要的字母，组成单词来表达意思。
 *
 * 给你一个赎金信 (ransomNote) 字符串和一个杂志(magazine)字符串，判断 ransomNote 能不能由 magazines 里面的字符构成。
 *
 * 如果可以构成，返回 true ；否则返回 false 。
 *
 * magazine 中的每个字符只能在 ransomNote 中使用一次。
 *
 * 输入：ransomNote = "a", magazine = "b"
 * 输出：false
 *
 * 输入：ransomNote = "aa", magazine = "ab"
 * 输出：false
 *
 * 输入：ransomNote = "aa", magazine = "aab"
 * 输出：true
 */
public class _383canConstruct {

    /**
     * 思考 首先简单解法，空间换时间
     * 执行用时：     * 20 ms     * , 在所有 Java 提交中击败了     * 5.11%     * 的用户
     * 内存消耗：     * 38.2 MB     * , 在所有 Java 提交中击败了     * 97.84%     * 的用户
     *
     * @param ransomNote
     * @param magazine
     * @return
     */
    public boolean canConstruct(String ransomNote, String magazine) {
        if(ransomNote.length()>magazine.length())return false;
        if(magazine.contains(ransomNote))return true;
        Map<Character,Integer> hash = new HashMap<>();
        char temp = ' ';
        // 首先将杂志字符串 存入到hash中
        for (int i = 0; i < magazine.length(); i++) {
            temp = magazine.charAt(i);
            if(hash.keySet().contains(temp)){
                hash.put(temp,hash.get(temp)+1);
            }else {
                hash.put(temp,1);
            }
        }
        // 遍历ransomNote
        for (int i = 0; i < ransomNote.length(); i++) {
            temp = ransomNote.charAt(i);
            if(hash.keySet().contains(temp)){
                if (hash.get(temp) >= 1) {
                    hash.put(temp,hash.get(temp)-1);
                }else {
                    return false;
                }
            }else {
                return false;
            }
        }
        return true;
    }

    /**
     * 题解解法 ：
     * 思路都是暴力统计字母个数，但用数组效率高。注意看 cnt[c-'a']-- .神思路，神代码！！！
     * 执行用时：     * 2 ms     * , 在所有 Java 提交中击败了     * 47.60%     * 的用户
     * 内存消耗：     * 38.4 MB     * , 在所有 Java 提交中击败了     * 88.39%     * 的用户
     * @param ransomNote
     * @param magazine
     * @return
     */
    public boolean canConstruct1(String ransomNote, String magazine) {
        if (ransomNote.length() > magazine.length()) {
            return false;
        }
        int[] cnt = new int[26];
        for (char c : magazine.toCharArray()) {
            cnt[c - 'a']++;
        }
        for (char c : ransomNote.toCharArray()) {
            cnt[c - 'a']--;
            if(cnt[c - 'a'] < 0) {
                return false;
            }
        }
        return true;
    }

}
