package code301_400.code80_90;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 给定一个字符串，找到它的第一个不重复的字符，并返回它的索引。如果不存在，则返回 -1。
 *
 * s = "leetcode"
 * 返回 0
 *
 * s = "loveleetcode"
 * 返回 2
 */
public class _387firstUniqChar {

    /**
     * 第一种方法，直接存储：
     * 执行用时：     * 25 ms     * , 在所有 Java 提交中击败了     * 49.17%     * 的用户
     * 内存消耗：     * 38.9 MB     * , 在所有 Java 提交中击败了     * 63.01%     * 的用户
     *
     * 第二种方法：使用队列
     * @param s
     * @return
     */
    public int firstUniqChar(String s) {
        Map<Character, Integer> frequency = new HashMap<>();
        for (int i = 0; i < s.length(); ++i) {
            char ch = s.charAt(i);
            frequency.put(ch, frequency.getOrDefault(ch, 0) + 1);
        }
        for (int i = 0; i < s.length(); ++i) {
            if (frequency.get(s.charAt(i)) == 1) {
                return i;
            }
        }
        return -1;
    }

}
