package code301_400.code80_90;

/**
 * 给定两个字符串 s 和 t，它们只包含小写字母。
 *
 * 字符串 t 由字符串 s 随机重排，然后在随机位置添加一个字母。
 *
 * 请找出在 t 中被添加的字母。
 *
 * 输入：s = "abcd", t = "abcde"
 * 输出："e"
 * 解释：'e' 是那个被添加的字母。
 *
 * 输入：s = "", t = "y"
 * 输出："y"
 *
 * 输入：s = "a", t = "aa"
 * 输出："a"
 *
 * 输入：s = "ae", t = "aea"
 * 输出："a"
 */
public class _389findTheDifference {

    /**
     * 执行用时：     * 3 ms     * , 在所有 Java 提交中击败了     * 48.47%     * 的用户
     * 内存消耗：     * 36.5 MB     * , 在所有 Java 提交中击败了     * 87.73%     * 的用户
     * @param s
     * @param t
     * @return
     */
    public char findTheDifference(String s, String t) {
        int[] charNums = new int[26];
        for (int i = 0; i < s.length(); i++) {
            ++charNums[s.charAt(i)-'a'];
        }
        int temp;
        for (int i = 0; i < t.length(); i++) {
            temp = t.charAt(i)-'a';
            if(charNums[temp]==0){
                return (char)('a'+temp);
            }else {
                --charNums[temp];
            }
        }
        return ' ';
    }

}
