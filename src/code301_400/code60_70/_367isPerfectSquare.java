package code301_400.code60_70;

/**
 * 给定一个 正整数 num ，编写一个函数，如果 num 是一个完全平方数，则返回 true ，否则返回 false 。
 *
 * 进阶：不要 使用任何内置的库函数，如  sqrt 。
 *
 * 输入：num = 16
 * 输出：true
 *
 * 输入：num = 14
 * 输出：false
 */
public class _367isPerfectSquare {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 35.2 MB     * , 在所有 Java 提交中击败了     * 88.68%     * 的用户
     * @param num
     * @return
     */
    public static boolean isPerfectSquare(int num) {
        int right = num/2 + 1;
        int left = 0;
        int mid;
        long res;
        while (left < right){
            mid = left + (right-left + 1)/2;
            res = (long)mid * mid;
            if(res == num){
                return true;
            }else if(res > num){
                right = mid - 1;
            }else {
                left = mid;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isPerfectSquare(808201));
    }

}
