package code301_400.code40_50;

/**
 * 编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 s 的形式给出。
 *
 * 不要给另外的数组分配额外的空间，你必须原地修改输入数组、使用 O(1) 的额外空间解决这一问题。
 *
 * 输入：s = ["h","e","l","l","o"]
 * 输出：["o","l","l","e","h"]
 *
 * 输入：s = ["H","a","n","n","a","h"]
 * 输出：["h","a","n","n","a","H"]
 */
public class _344reverseString {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 86.33%     * 的用户
     * 内存消耗：     * 45.3 MB     * , 在所有 Java 提交中击败了     * 74.49%     * 的用户
     * @param s
     */
    public void reverseString(char[] s) {
        if(s.length==1)return;
        char temp = ' ';
        for (int i = 0; i < s.length / 2; i++) {
            temp = s[i];
            s[i] = s[s.length-i-1];
            s[s.length-i-1] = temp;
        }
    }

}
