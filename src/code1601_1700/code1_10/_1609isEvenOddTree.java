package code1601_1700.code1_10;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * 如果一棵二叉树满足下述几个条件，则可以称为 奇偶树 ：
 *
 *     二叉树根节点所在层下标为 0 ，根的子节点所在层下标为 1 ，根的孙节点所在层下标为 2 ，依此类推。
 *     偶数下标 层上的所有节点的值都是 奇 整数，从左到右按顺序 严格递增
 *     奇数下标 层上的所有节点的值都是 偶 整数，从左到右按顺序 严格递减
 *
 * 给你二叉树的根节点，如果二叉树为 奇偶树 ，则返回 true ，否则返回 false 。
 *
 * 输入：root = [1,10,4,3,null,7,9,12,8,6,null,null,2]
 * 输出：true
 * 解释：每一层的节点值分别是：
 * 0 层：[1]
 * 1 层：[10,4]
 * 2 层：[3,7,9]
 * 3 层：[12,8,6,2]
 * 由于 0 层和 2 层上的节点值都是奇数且严格递增，而 1 层和 3 层上的节点值都是偶数且严格递减，因此这是一棵奇偶树。
 *
 * 输入：root = [5,4,2,3,3,7]
 * 输出：false
 * 解释：每一层的节点值分别是：
 * 0 层：[5]
 * 1 层：[4,2]
 * 2 层：[3,3,7]
 * 2 层上的节点值不满足严格递增的条件，所以这不是一棵奇偶树。
 *
 * 输入：root = [5,9,1,3,5,7]
 * 输出：false
 * 解释：1 层上的节点值应为偶数。
 *
 * 输入：root = [1]
 * 输出：true
 *
 * 输入：root = [11,8,6,1,3,9,11,30,20,18,16,12,10,4,2,17]
 * 输出：true
 */
public class _1609isEvenOddTree {

    /**
     * 执行用时：12 ms, 在所有 Java 提交中击败了48.63% 的用户
     * 内存消耗：54 MB, 在所有 Java 提交中击败了85.47% 的用户
     * @param root
     * @return
     */
    public boolean isEvenOddTree(TreeNode root) {
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.offer(root);
        TreeNode node;
        // 记录每次循环层数
        int size = 0;
        // 记录当前最大值或最小值
        int nowNum = 0;
        // 用来记录当前层数
        int level = 0;
        while (!queue.isEmpty()){
            size = queue.size();
            node = queue.poll();
            nowNum = node.val;
            if((nowNum+level)%2==0)return false;
            if(node.left!=null)queue.offer(node.left);
            if(node.right!=null)queue.offer(node.right);
            for (int i = 1; i < size; i++) {
                node = queue.poll();
                if(level%2==0&&node.val%2==1){
                    if(node.val>nowNum){
                        nowNum = node.val;
                    }else {
                        return false;
                    }
                }else if(level%2==1&&node.val%2==0){
                    if(node.val<nowNum){
                        nowNum = node.val;
                    }else {
                        return false;
                    }
                }else{
                    return false;
                }
                if(node.left!=null)queue.offer(node.left);
                if(node.right!=null)queue.offer(node.right);
            }
            ++level;
        }
        return true;
    }

}
