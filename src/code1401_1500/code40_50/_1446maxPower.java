package code1401_1500.code40_50;

/**
 * 给你一个字符串 s ，字符串的「能量」定义为：只包含一种字符的最长非空子字符串的长度。
 *
 * 请你返回字符串的能量。
 *
 * 输入：s = "leetcode"
 * 输出：2
 * 解释：子字符串 "ee" 长度为 2 ，只包含字符 'e' 。
 *
 * 输入：s = "abbcccddddeeeeedcba"
 * 输出：5
 * 解释：子字符串 "eeeee" 长度为 5 ，只包含字符 'e' 。
 *
 * 输入：s = "triplepillooooow"
 * 输出：5
 *
 * 输入：s = "hooraaaaaaaaaaay"
 * 输出：11
 *
 * 输入：s = "tourist"
 * 输出：1
 */
public class _1446maxPower {

    /**
     * 指针正常遍历即可
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.6 MB     * , 在所有 Java 提交中击败了     * 99.81%     * 的用户
     * @param s
     * @return
     */
    public int maxPower(String s) {
        int result = 1;
        int temp = 1;
        int left = 1;
        while (left<s.length()){
            if(s.charAt(left-1)==s.charAt(left)){
                ++left;
                ++temp;
            }else {
                temp = 1;
                ++left;
            }
            result = result>temp?result:temp;
        }
        return result;
    }

}
