package code101_200.code1_10;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 给你一个二叉树，请你返回其按 层序遍历 得到的节点值。 （即逐层地，从左到右访问所有节点）。
 */
public class _102levelOrder {

    /**
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 90.13%     * 的用户
     * 内存消耗：     * 38.7 MB     * , 在所有 Java 提交中击败了     * 20.94%     * 的用户
     * @param root
     * @return
     */
    public List<List<Integer>> levelOrder(TreeNode root) {
        // 创建返回结果集
        List<List<Integer>> result = new ArrayList<>();
        if(root==null)return result;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            int size = queue.size();
            List<Integer> nodeList = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                nodeList.add(node.val);
                if(node.left!=null)queue.offer(node.left);
                if(node.right!=null)queue.offer(node.right);
            }
            result.add(nodeList);
        }
        return result;
    }

}
