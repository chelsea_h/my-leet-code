package code101_200.code1_10;

import java.util.Stack;

/**
 * 给定一个二叉树，检查它是否是镜像对称的。
 * 例如，二叉树 [1,2,2,3,4,4,3] 是对称的。
 *
 */
public class _101isSymmetricTree {

    /**
     * 每次入栈两个节点，如果节点左节点存在，则将另一个节点的右节点也存进去。
     * 然后就是普通的遍历整个二叉树了。
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 22.31%     * 的用户
     * 内存消耗：     * 37.8 MB     * , 在所有 Java 提交中击败了     * 12.69%     * 的用户
     *
     * 注释内的为初始代码，注释外的为试的优化代码，但优化结果毫无作用，看来得从方法级别优化，先暂定
     *
     * 进一步思考:也可以进行层次遍历,对每一层的节点进行入栈.每一层n/2的循环判断栈是否对称即可
     * @param root
     * @return
     */
    public boolean isSymmetric(TreeNode root) {
        if(root==null)return false;
        if(root.right==null&&root.left==null)return true;
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode node1;
        TreeNode node2;
        stack.push(root);
        stack.push(root);
        while (stack.size()!=0){
            node1 = stack.pop();
            node2 = stack.pop();
            if(node1==null&&node2==null){
                continue;
            }
            if(node1==null||node2==null){
                return false;
            }
            if(node1.val!=node2.val){
                return false;
            }
            if(node1.left==null&&node2.right==null&&node1.right==null&&node2.left==null){
                continue;
            }
            if((node1.left==null&&node2.right!=null)||(node1.right==null&&node2.left!=null)){
                return false;
            }
            stack.push(node1.left);
            stack.push(node2.right);

            stack.push(node1.right);
            stack.push(node2.left);

           /* if(node1.left!=null){
                stack.push(node1.left);
                if(node2.right!=null){
                    stack.push(node2.right);
                }else {
                    return false;
                }
            }
            if(node1.right!=null){
                stack.push(node1.right);
                if(node2.left!=null){
                    stack.push(node2.left);
                }else {
                    return false;
                }
            }*/
        }
        return true;
    }

    public static void main(String[] args) {

    }
}
