package code101_200.code60_70;

/**
 * 给你一个整数 columnNumber ，返回它在 Excel 表中相对应的列名称。
 * A -> 1
 * B -> 2
 * C -> 3
 * ...
 * Z -> 26
 * AA -> 27
 * AB -> 28
 *
 * 输入：columnNumber = 1
 * 输出："A"
 *
 * 输入：columnNumber = 28
 * 输出："AB"
 *
 * 输入：columnNumber = 2147483647
 * 输出："FXSHRXW"
 */
public class _168convertToTitle {

    /**
     * 思考 ： 相当于10进制转26进制。其中A-Z的ascii码为：65-90。但要注意边界问题！
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 35.5 MB     * , 在所有 Java 提交中击败了     * 75.76%     * 的用户
     *
     * @param columnNumber
     * @return
     */
    public static String convertToTitle(int columnNumber) {
        StringBuffer sb = new StringBuffer();
        while (columnNumber > 0) {
            int a0 = (columnNumber - 1) % 26 + 1;
            sb.append((char)(a0 - 1 + 'A'));
            columnNumber = (columnNumber - a0) / 26;
        }
        return sb.reverse().toString();
    }

    public static void main(String[] args) {
        System.out.println(convertToTitle(52));
    }

}
