package code101_200.code60_70;

/**
 * 给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。
 *
 * 你可以假设数组是非空的，并且给定的数组总是存在多数元素。
 *
 * 输入：[3,2,3]
 * 输出：3
 *
 */
public class _169majorityElement {

    /**
     * 摩尔投票法：
     *
     * 核心就是对拼消耗。
     *
     * 玩一个诸侯争霸的游戏，假设你方人口超过总人口一半以上，并且能保证每个人口出去干仗都能一对一同归于尽。最后还有人活下来的国家就是胜利。
     *
     * 那就大混战呗，最差所有人都联合起来对付你（对应你每次选择作为计数器的数都是众数），或者其他国家也会相互攻击（会选择其他数作为计数器的数），但是只要你们不要内斗，最后肯定你赢。
     *
     * 最后能剩下的必定是自己人。
     *
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 99.96%     * 的用户
     * 内存消耗：     * 44.3 MB     * , 在所有 Java 提交中击败了     * 70.32%     * 的用户
     *
     * @param nums
     * @return
     */
    public static int majorityElement(int[] nums) {
        int res = nums[0];
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if(res == nums[i]){
                count++;
            }else if(count!=0){
                count--;
            }else {
                res = nums[i];
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(majorityElement(new int[]{2,2,1,1,1,2,2}));
    }

}
