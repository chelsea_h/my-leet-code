package code101_200.code20_30;

/**
 * 买股票的最佳时间
 * 给定一个数组 prices ，它的第 i 个元素 prices[i] 表示一支给定股票第 i 天的价格。
 *
 * 你只能选择 某一天 买入这只股票，并选择在 未来的某一个不同的日子 卖出该股票。设计一个算法来计算你所能获取的最大利润。
 *
 * 返回你可以从这笔交易中获取的最大利润。如果你不能获取任何利润，返回 0 。
 *
 * 输入：[7,1,5,3,6,4]
 * 输出：5
 * 解释：在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
 *      注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格；同时，你不能在买入前卖出股票。
 *
 *
 */
public class _121buyStock {

    /**
     * 普通的数字递推.
     *
     * 思考：因此，我们只需要遍历价格数组一遍，记录历史最低点，然后在每一天考虑这么一个问题：如果我是在历史最低点买进的，那么我今天卖出能赚多少钱？当考虑完所有天数之时，我们就得到了最好的答案
     * 注：历史低点表示的是i-（i-1）天的历史低点，而不是全局的历史低点
     *
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 51.3 MB     * , 在所有 Java 提交中击败了     * 86.39%     * 的用户
     *
     * @param prices
     * @return
     */
    public static int maxProfit0(int[] prices) {
        if(prices.length==1) return 0;
        //用来记录目前最大收益
        int max = 0;
        //用来记录历史低点
        int minPrice = prices[0];
        for (int i = 1; i < prices.length; i++) {
            if(minPrice<prices[i]){
                if(max<prices[i]-minPrice)max = prices[i]-minPrice;
            }else {
                minPrice = prices[i];
            }
        }
        return max;
    }

    /**
     * 二刷
     * 执行用时：     * 2 ms     * , 在所有 Java 提交中击败了     * 92.07%     * 的用户
     * 内存消耗：     * 51.2 MB     * , 在所有 Java 提交中击败了     * 68.64
     * 的用户
     * @param prices
     * @return
     */
    public static int maxProfit(int[] prices) {
        int profit = 0;
        int minPrice = prices[0];
        if(prices.length==1)return 0;
        for (int i = 0; i < prices.length; i++) {
            if(prices[i]<minPrice){
                minPrice = prices[i];
            }
            profit = Math.max(prices[i]-minPrice,profit);
        }
        return profit;
    }

    public static void main(String[] args) {
        System.out.println(maxProfit(new int[]{7,1,5,3,6,4}));
    }
}
