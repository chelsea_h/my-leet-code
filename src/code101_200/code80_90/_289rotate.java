package code101_200.code80_90;

/**
 * 给你一个数组，将数组中的元素向右轮转 k 个位置，其中 k 是非负数
 *
 * 输入: nums = [1,2,3,4,5,6,7], k = 3
 * 输出: [5,6,7,1,2,3,4]
 * 解释:
 * 向右轮转 1 步: [7,1,2,3,4,5,6]
 * 向右轮转 2 步: [6,7,1,2,3,4,5]
 * 向右轮转 3 步: [5,6,7,1,2,3,4]
 *
 * 输入：nums = [-1,-100,3,99], k = 2
 * 输出：[3,99,-1,-100]
 * 解释:
 * 向右轮转 1 步: [99,-1,-100,3]
 * 向右轮转 2 步: [3,99,-1,-100]
 *
 */
public class _289rotate {

    /**
     * 方法1：空间换时间，新建一个同样大小数组，求得最终k后，分两步将数据copy进去即可
     * 方法2：考虑空间复杂度为1的方法，再数组内部轮换。
     *        具体的，先翻转所有数组，求得k之后，翻转0-k-1个，再翻转k-n个元素即可。翻转使用单指针遍历一遍即可
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 64.48%     * 的用户
     * 内存消耗：     * 55.3 MB     * , 在所有 Java 提交中击败了     * 95.57%     * 的用户
     * @param nums
     * @param k
     */
    public static void rotate(int[] nums, int k) {
        k = k%nums.length;
        if(k==0)return;
        reverse(nums,0,nums.length-1);
        reverse(nums,0,k-1);
        reverse(nums, k,nums.length-1);
    }

    public static void reverse(int[] nums,int start, int end){
        int temp = 0;
        while (start<end){
            temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }

    /**
     * 上述的方法一
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 64.48%     * 的用户
     * 内存消耗：     * 54.9 MB     * , 在所有 Java 提交中击败了     * 90.66%     * 的用户
     * @param nums
     * @param k
     */
    public void rotate0(int[] nums, int k) {
        int n = nums.length;
        int[] newArr = new int[n];
        for (int i = 0; i < n; ++i) {
            newArr[(i + k) % n] = nums[i];
        }
        System.arraycopy(newArr, 0, nums, 0, n);
    }

}
