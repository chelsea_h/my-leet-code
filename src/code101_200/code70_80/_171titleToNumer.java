package code101_200.code70_80;

/**
 * 给你一个字符串 columnTitle ，表示 Excel 表格中的列名称。返回该列名称对应的列序号。
 *
 * 输入: columnTitle = "A"
 * 输出: 1
 *
 * 输入: columnTitle = "AB"
 * 输出: 28
 *
 * 输入: columnTitle = "ZY"
 * 输出: 701
 *
 */
public class _171titleToNumer {

    /**
     * 还是和之前一样 就是涉及一个进制转换的问题
     *
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38.6 MB     * , 在所有 Java 提交中击败了     * 5.16%     * 的用户
     *
     * 去掉中间变量后，代码变得生涩难懂，空间效率稍有提高（最终需要提升的话，估计得去掉 Math 库）
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38.5 MB     * , 在所有 Java 提交中击败了     * 22.82%     * 的用户
     *
     * @param columnTitle
     * @return
     */
    public int titleToNumber(String columnTitle) {
        if("".equals(columnTitle))return 0;
        int result = 0;
        for (int i = 0; i < columnTitle.length(); i++) {
            result += ((int)(columnTitle.charAt(columnTitle.length()-i-1)) - 64)*Math.pow(26,i);
        }
        return result;
    }

}
