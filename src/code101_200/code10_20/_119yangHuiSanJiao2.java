package code101_200.code10_20;

import java.util.ArrayList;
import java.util.List;

/**
 * 杨辉三角2
 * 给定一个非负索引 rowIndex，返回「杨辉三角」的第 rowIndex 行。
 *
 * 在「杨辉三角」中，每个数是它左上方和右上方的数的和。
 *
 * 输入: rowIndex = 3
 * 输出: [1,3,3,1]
 *
 */
public class _119yangHuiSanJiao2 {

    /**
     * 思考，可参考上一题，最终输出第N行即可。
     * 但直接套用上一题 空间复杂度有点高，这次只需要创建两行数组即可
     *
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 78.73%     * 的用户
     * 内存消耗：     * 36 MB     * , 在所有 Java 提交中击败了     * 66.66%     * 的用户
     *
     * @param rowIndex
     * @return
     */
    public static List<Integer> getRow(int rowIndex) {
        List<Integer> arr = new ArrayList<>();
        if(rowIndex==0){
            arr.add(1);
            return arr;
        }

        for (int i = 0; i < rowIndex; i++) {
            List<Integer> tempInFor = new ArrayList<>();
            tempInFor.add(1);
            for (int j = 0; j < i; j++) {
                tempInFor.add(arr.get(j)+arr.get(j+1));
            }
            tempInFor.add(1);
            arr = tempInFor;
        }
        return arr;
    }

    public static void main(String[] args) {
        for (Integer o :
                getRow(1)) {
            System.out.print(o+"  ");
        }
    }

}
