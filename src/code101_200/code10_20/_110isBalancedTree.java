package code101_200.code10_20;

/**
 * 给定一个二叉树，判断它是否是高度平衡的二叉树。
 *
 * 本题中，一棵高度平衡二叉树定义为：一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过 1 。
 *
 * 输入：root = [3,9,20,null,null,15,7]
 * 输出：true
 */
public class _110isBalancedTree {

    /**
     * 考虑：动态规划子问题。递归最好实现。判断两边高度差小于一且都平衡。
     *
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 70.45%     * 的用户
     * 内存消耗：     * 38.3 MB     * , 在所有 Java 提交中击败了     * 82.34%     * 的用户
     *
     * @param root
     * @return
     */
    public boolean isBalanced(TreeNode root) {
        if(root==null){
            return true;
        }else if(Math.abs(height(root.left)-height(root.right))>1){
            return false;
        }else if(isBalanced(root.left)&&isBalanced(root.right)){
            return true;
        }else {
            return false;
        }
    }

    public int height(TreeNode node){
        if(node==null){
            return 0;
        }else {
//            return height(node.left)>height(node.right)?height(node.left)+1:height(node.right)+1;
            return Math.max(height(node.left), height(node.right)) + 1;
        }
    }

}

