package code101_200.code10_20;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 给定一个 完美二叉树 ，其所有叶子节点都在同一层，每个父节点都有两个子节点。二叉树定义如下：
 * struct Node {
 *   int val;
 *   Node *left;
 *   Node *right;
 *   Node *next;
 * }
 *
 * 填充它的每个 next 指针，让这个指针指向其下一个右侧节点。如果找不到下一个右侧节点，则将 next 指针设置为 NULL。
 *
 * 初始状态下，所有 next 指针都被设置为 NULL。
 *
 * 你只能使用常量级额外空间。
 * 使用递归解题也符合要求，本题中递归程序占用的栈空间不算做额外的空间复杂度。
 *
 * 输入：root = [1,2,3,4,5,6,7]
 * 输出：[1,#,2,3,#,4,5,6,7,#]
 * 解释：给定二叉树如图 A 所示，你的函数应该填充它的每个 next 指针，以指向其下一个右侧节点，如图 B 所示。序列化的输出按层序遍历排列，同一层节点由 next 指针连接，'#' 标志着每一层的结束。
 *
 */
public class _116connect {

    /**
     * 思考：使用层序遍历。此方法空间复杂度较高，不满足进阶的O1空间复杂度的要求。
     * 执行用时：     * 3 ms     * , 在所有 Java 提交中击败了     * 35.06%     * 的用户
     * 内存消耗：     * 38.4 MB     * , 在所有 Java 提交中击败了     * 88.46%     * 的用户
     * @param root
     * @return
     */
    public Node connect(Node root) {
        if(root==null)return null;
        if(root.left==null&&root.right==null)return root;
        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        Node node1;
        Node node2;
        while (!queue.isEmpty()){
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                node1 = queue.poll();
                node2 = queue.peek();
                node1.next = node2;
                if(i==size-1) node1.next=null;
                if(node1.left!=null)queue.offer(node1.left);
                if(node1.right!=null)queue.offer(node1.right);
            }
        }
        return root;
    }


    /**
     * 第二章方法：使用已建立的Next指针。(因为是完全树才可以这样)
     * 1. 第一种情况是连接同一个父节点的两个子节点。它们可以通过同一个节点直接访问到，因此执行下面操作即可完成连接。
     * 2. 第二种情况在不同父亲的子节点之间建立连接，这种情况不能直接连接。
     * 如果每个节点有指向父节点的指针，可以通过该指针找到 \text{next}next 节点。如果不存在该指针，则按照下面思路建立连接：
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38.5 MB     * , 在所有 Java 提交中击败了     * 74.12%     * 的用户
     * @param root
     * @return
     */
    public Node connect2(Node root) {
        if (root == null)return null;
        // 从根节点开始
        Node leftmost = root;
        while (leftmost.left != null) {
            // 遍历这一层节点组织成的链表，为下一层的节点更新 next 指针
            Node head = leftmost;
            while (head != null) {
                // CONNECTION 1
                head.left.next = head.right;
                // CONNECTION 2
                if (head.next != null) {
                    head.right.next = head.next.left;
                }
                // 指针向后移动
                head = head.next;
            }
            // 去下一层的最左的节点
            leftmost = leftmost.left;
        }
        return root;
    }
}
