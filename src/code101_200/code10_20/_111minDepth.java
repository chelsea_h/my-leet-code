package code101_200.code10_20;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * 二叉树的最小深度
 * 给定一个二叉树，找出其最小深度。
 *
 * 最小深度是从根节点到最近叶子节点的最短路径上的节点数量。
 *
 * 说明：叶子节点是指没有子节点的节点。
 *
 * 输入：root = [3,9,20,null,null,15,7]
 * 输出：2
 *
 */
public class _111minDepth {

    /**
     * 思考：和最大深度一样，层序遍历可直接解决此问题
     * 最大深度：知道走完所有节点，返回最终深度值
     * 最小深度：层序遍历，判断某个节点没有左子树和右子树，直接break返回层数即可
     *
     * 一次跑通
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 99.25%     * 的用户
     * 内存消耗：     * 58.1 MB     * , 在所有 Java 提交中击败了     * 79.18%     * 的用户
     *
     * @param root
     * @return
     */
    public int minDepth(TreeNode root) {
        Deque<TreeNode> deque = new ArrayDeque<>();
        if(root==null) return 0;
        int depth = 0;
        int size = 0;
        deque.add(root);
        TreeNode node;
        while (deque.size()!=0){
            depth++;
            size = deque.size();
            for (int i = 0; i < size; i++) {
                node = deque.pop();
                if(node.left==null&&node.right==null) return depth;
                if (node.left!=null) deque.add(node.left);
                if (node.right!=null) deque.add(node.right);
            }
        }
        return depth;
    }

}
