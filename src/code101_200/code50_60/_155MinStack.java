package code101_200.code50_60;

import java.util.ArrayList;
import java.util.List;

/**
 * 设计一个支持 push ，pop ，top 操作，并能在常数时间内检索到最小元素的栈。
 *
 * push(x) —— 将元素 x 推入栈中。
 * pop() —— 删除栈顶的元素。
 * top() —— 获取栈顶元素。
 * getMin() —— 检索栈中的最小元素。
 *
 * 输入：
 * ["MinStack","push","push","push","getMin","pop","top","getMin"]
 * [[],[-2],[0],[-3],[],[],[],[]]
 *
 * 输出：
 * [null,null,null,null,-3,null,0,-2]
 *
 * 解释：
 * MinStack minStack = new MinStack();
 * minStack.push(-2);
 * minStack.push(0);
 * minStack.push(-3);
 * minStack.getMin();   --> 返回 -3.
 * minStack.pop();
 * minStack.top();      --> 返回 0.
 * minStack.getMin();   --> 返回 -2.
 *
 */

/**
 *
 * 一次通过！
 *
 * 执行用时： * 4 ms * , 在所有 Java 提交中击败了 * 99.63% * 的用户
 * 内存消耗： * 40.1 MB * , 在所有 Java 提交中击败了 * 81.38% * 的用户
 */
public class _155MinStack {

    int minVal;
    List<Integer> list;

    public _155MinStack() {
        minVal = -1;
        list = new ArrayList();
    }

    public void push(int val) {
        if(minVal==-1)minVal = 0;
        list.add(val);
        if(val<list.get(minVal)) minVal = list.size()-1;
    }

    //删除栈顶元素，删除时要更新最小元素位置，不然万一把最小元素删除了就坏了
    public void pop() {
        if(list.size()==0) return;
        if(list.size()==1) minVal = -1;
        if(minVal==list.size()-1){
            //寻找除最后一位的最小元素。
            int min = 0;
            for (int i = 0; i < list.size() - 1; i++) {
                if(list.get(i)<list.get(min)){
                    min = i;
                }
            }
            //更新最小元素下标索引
            minVal = min;
        }
        list.remove(list.size()-1);
    }

    public int top() {
        return list.get(list.size()-1);
    }

    public int getMin() {
        return list.get(minVal);
    }

}
