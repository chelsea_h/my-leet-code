package code2201_2300.code10_20;

/**
 * 一个长度为n-1的递增排序数组中的所有数字都是唯一的，并且每个数字都在范围0～n-1之内。
 * 在范围0～n-1内的n个数字中有且只有一个数字不在该数组中，请找出这个数字。
 *
 * 输入: [0,1,3]
 * 输出: 2
 *
 * 输入: [0,1,2,3,4,5,6,7,9]
 * 输出: 8
 */
public class _2213missingNumber {

    /**
     * 简单方法，直接便利。
     * 进阶方法：二分查找
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38.7 MB     * , 在所有 Java 提交中击败了     * 80.78%     * 的用户
     * @param nums
     * @return
     */
    public int missingNumber(int[] nums) {
        if(nums.length==1)return 2-nums[0]-1;
        int left = 0;
        int right = nums.length-1;
        int mid;
        while (left<right){
            mid = left+(right-left)/2;
            if(nums[mid] == mid){
                left = mid + 1;
            }else if(nums[mid]>mid){
                right = mid;
            }
        }
        if(nums[left] == left)return nums[left]+1;
        return nums[left]-1;
    }

}
