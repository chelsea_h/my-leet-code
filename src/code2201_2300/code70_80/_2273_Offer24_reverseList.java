package code2201_2300.code70_80;

/**
 * 给定单链表的头节点 head ，请反转链表，并返回反转后的链表的头节点。
 * 输入：head = [1,2,3,4,5]
 * 输出：[5,4,3,2,1]
 *
 */
public class _2273_Offer24_reverseList {

    /**
     * 头插法反转链表即可
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 37.9 MB     * , 在所有 Java 提交中击败了     * 93.14%     * 的用户
     * @param head
     * @return
     */
    public ListNode reverseList(ListNode head) {
        if(head==null||head.next==null)return head;
        ListNode node;
        ListNode result = null;
        while (head!=null){
            node = head;
            head = head.next;

            node.next = result;
            result = node;
        }
        return result;
    }

}
