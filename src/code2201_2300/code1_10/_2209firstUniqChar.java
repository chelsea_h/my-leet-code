package code2201_2300.code1_10;

/**
 * 在字符串 s 中找出第一个只出现一次的字符。如果没有，返回一个单空格。 s 只包含小写字母。
 *
 * 输入：s = "abaccdeff"
 * 输出：'b'
 *
 * 输入：s = ""
 * 输出：' '
 */
public class _2209firstUniqChar {

    /**
     * 执行用时：     * 6 ms     * , 在所有 Java 提交中击败了     * 85.77%     * 的用户
     * 内存消耗：     * 38.7 MB     * , 在所有 Java 提交中击败了     * 51.04%     * 的用户
     * @param s
     * @return
     */
    public char firstUniqChar(String s) {
        if(s.length()==0)return ' ';
        int[] chars = new int[26];
        for (int i = 0; i < s.length(); i++) {
            ++chars[s.charAt(i)-'a'];
        }
        for (int i = 0; i < s.length(); i++) {
            if(chars[s.charAt(i)-'a']==1){
                return s.charAt(i);
            }
        }
        return ' ';
    }

}
