package code2201_2300.code60_70;

/**
 * 给定一个已按照 升序排列  的整数数组 numbers ，请你从数组中找出两个数满足相加之和等于目标数 target 。
 *
 * 函数应该以长度为 2 的整数数组的形式返回这两个数的下标值。numbers 的下标 从 0 开始计数 ，所以答案数组应当满足 0 <= answer[0] < answer[1] < numbers.length 。
 *
 * 假设数组中存在且只存在一对符合条件的数字，同时一个数字不能使用两次。
 *
 * 输入：numbers = [1,2,4,6,10], target = 8
 * 输出：[1,3]
 * 解释：2 与 6 之和等于目标数 8 。因此 index1 = 1, index2 = 3 。
 *
 * 输入：numbers = [2,3,4], target = 6
 * 输出：[0,2]
 *
 * 输入：numbers = [-1,0], target = -1
 * 输出：[0,1]
 */
public class _2260_Offer6_twoSum {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38.7 MB     * , 在所有 Java 提交中击败了     * 56.64%     * 的用户
     * @param numbers
     * @param target
     * @return
     */
    public int[] twoSum(int[] numbers, int target) {
        int left = 0;
        int right = numbers.length-1;
        if(target<numbers[0])return null;
        int add;
        while (left < right){
            add = numbers[left] + numbers[right];
            if(add == target){
                return new int[]{left,right};
            }else if(add < target){
                ++left;
            }else {
                --right;
            }
        }
        return null;
    }

}
