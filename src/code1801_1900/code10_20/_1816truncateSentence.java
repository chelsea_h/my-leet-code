package code1801_1900.code10_20;

/**
 * 句子 是一个单词列表，列表中的单词之间用单个空格隔开，且不存在前导或尾随空格。每个单词仅由大小写英文字母组成（不含标点符号）。
 *
 * 例如，"Hello World"、"HELLO" 和 "hello world hello world" 都是句子。
 * 给你一个句子 s一个整数 k，请你将 s截断 ，使截断后的句子仅含 前k个单词。返回截断 s​后得到的句子。
 *
 * 输入：s = "Hello how are you Contestant", k = 4
 * 输出："Hello how are you"
 * 解释：
 * s 中的单词为 ["Hello", "how" "are", "you", "Contestant"]
 * 前 4 个单词为 ["Hello", "how", "are", "you"]
 * 因此，应当返回 "Hello how are you"
 *
 * 输入：s = "What is the solution to this problem", k = 4
 * 输出："What is the solution"
 * 解释：
 * s 中的单词为 ["What", "is" "the", "solution", "to", "this", "problem"]
 * 前 4 个单词为 ["What", "is", "the", "solution"]
 * 因此，应当返回 "What is the solution"
 *
 * 输入：s = "chopper is not a tanuki", k = 5
 * 输出："chopper is not a tanuki"
 */
public class _1816truncateSentence {

    /**
     * 执行用时：     * 2 ms     * , 在所有 Java 提交中击败了     * 65.28%     * 的用户
     * 内存消耗：     * 36.7 MB     * , 在所有 Java 提交中击败了     * 55.26%     * 的用户
     *
     * 直接调库函数有点暴力，可以直接使用for遍历空格数
     * @param s
     * @param k
     * @return
     */
    public String truncateSentence(String s, int k) {
        String[] sentence = s.split(" ");
        if(sentence.length==1&&k==1)return s;
        if(sentence.length<=k)return s;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < k; i++) {
            stringBuilder.append(sentence[i]);
            stringBuilder.append(" ");
        }
        return stringBuilder.toString().substring(0,stringBuilder.length()-1);
    }

    /**
     * 果然，此方法确实不适合调用库函数
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.3 MB     * , 在所有 Java 提交中击败了     * 91.69%     * 的用户
     * @param s
     * @param k
     * @return
     */
    public String truncateSentence1(String s, int k) {
        char temp = ' ';
        for (int i = 0; i < s.length(); i++) {
            temp = s.charAt(i);
            if(temp==' '){
                k--;
            }
            if(k==0){
                return s.substring(0,i);
            }
        }
        return s;
    }

}
