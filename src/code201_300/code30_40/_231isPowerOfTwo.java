package code201_300.code30_40;

/**
 * 给你一个整数 n，请你判断该整数是否是 2 的幂次方。如果是，返回 true ；否则，返回 false 。
 *
 * 如果存在一个整数 x 使得 n == 2x ，则认为 n 是 2 的幂次方。
 *
 *  输入：n = 1
 * 输出：true
 * 解释：20 = 1
 *
 * 输入：n = 16
 * 输出：true
 * 解释：24 = 16
 *
 * 输入：n = 3
 * 输出：false
 */
public class _231isPowerOfTwo {

    /**
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 86.28%     * 的用户
     * 内存消耗：     * 35.2 MB     * , 在所有 Java 提交中击败了     * 90.37%     * 的用户
     * @param n
     * @return
     */
    public boolean isPowerOfTwo(int n) {
        if(n<=0)return false;
        while (n>1){
            if(n%2==1) {
                return false;
            }else {
                n = n/2;
            }
        }
        return true;
    }

}
