package code201_300.code1_10;

import java.util.HashMap;
import java.util.Map;

/**
 * 给定两个字符串 s 和 t，判断它们是否是同构的。
 *
 * 如果 s 中的字符可以按某种映射关系替换得到 t ，那么这两个字符串是同构的。
 *
 * 每个出现的字符都应当映射到另一个字符，同时不改变字符的顺序。不同字符不能映射到同一个字符上，相同字符只能映射到同一个字符上，字符可以映射到自己本身。
 *
 * 输入：s = "egg", t = "add"
 * 输出：true
 *
 * 输入：s = "foo", t = "bar"
 * 输出：false
 */
public class _205isIsomorphic {

    /**
     * 提示：可以假设s和t长度相同
     * 思考：好像只能按照键值对进行存储一一验证，但效率肯定会低
     * 执行用时：     * 11 ms     * , 在所有 Java 提交中击败了     * 77.19%     * 的用户
     * 内存消耗：     * 38.2 MB     * , 在所有 Java 提交中击败了     * 81.62%     * 的用户
     * 超出意料，空间效率还OK。时间效率待提升。提升时间效率的话，集合肯定就少用了，自己写数据结构
     * @param s
     * @param t
     * @return
     */
    public boolean isIsomorphic(String s, String t) {
        if(s.equals(t))return true;
        if(s==null&&t==null)return true;
        char temp = ' ';
        Map<Character,Character> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            temp = s.charAt(i);
            if(map.containsKey(temp)){
                if(t.charAt(i)==map.get(temp)){
                    continue;
                }else {
                    return false;
                }
            }else if(!map.containsValue(t.charAt(i))){
                map.put(s.charAt(i),t.charAt(i));
            }else {
                return false;
            }
        }
        return true;
    }

}
