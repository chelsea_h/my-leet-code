package code201_300.code1_10;

/**
 * 移除链表元素
 * 给你一个链表的头节点 head 和一个整数 val ，请你删除链表中所有满足 Node.val == val 的节点，并返回 新的头节点 。
 *
 * 输入：head = [1,2,6,3,4,5,6], val = 6
 * 输出：[1,2,3,4,5]
 *
 * 输入：head = [], val = 1
 * 输出：[]
 */
public class _203removeElements {

    /**
     * 执行用时：     * 1 ms     * , 在所有 Java 提交中击败了     * 96.25%     * 的用户
     * 内存消耗：     * 39.4 MB     * , 在所有 Java 提交中击败了     * 42.41%     * 的用户
     * @param head
     * @param val
     * @return
     */
    public static ListNode removeElements(ListNode head, int val) {
        if(head==null)return null;
        // 过滤掉前面头结点相等的值节点
        while (head!=null){
            if(head.val == val){
                head = head.next;
            }else {
                break;
            }
        }
        ListNode result = head;
        // 用来保存前一个结点
        ListNode pre = head;
        while (head!=null){
            if (head.val == val){
                pre.next = head.next;
            }else {
                pre = head;
            }
            head = head.next;
        }
        return result;
    }

    public static void main(String[] args) {
        ListNode node = new ListNode();
        node.val = 7;
        System.out.println(removeElements(node,7));
    }

}
