package code201_300.code1_10;

/**
 * 给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
 * 输入：head = [1,2,3,4,5]
 * 输出：[5,4,3,2,1]
 */
public class _206reverseList {

    /**
     * 思考：直接用头插法建立链表即可
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 38.1 MB     * , 在所有 Java 提交中击败了     * 76.91%     * 的用户
     *
     * @param head
     * @return
     */
    public ListNode reverseList(ListNode head) {
        if(head==null||head.next==null)return head;
        ListNode result = new ListNode();
        ListNode temp;
        while (head!=null){
            // 获取到即将新插入的节点
            temp = head;
            head = head.next;
            // 头插法创建链表
            temp.next = result.next;
            result.next = temp;
        }
        return result.next;
    }

}
