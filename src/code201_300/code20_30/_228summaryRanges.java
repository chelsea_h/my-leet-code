package code201_300.code20_30;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个无重复元素的有序整数数组 nums 。
 *
 * 返回 恰好覆盖数组中所有数字 的 最小有序 区间范围列表。也就是说，nums 的每个元素都恰好被某个区间范围所覆盖，并且不存在属于某个范围但不属于 nums 的数字 x 。
 *
 * 列表中的每个区间范围 [a,b] 应该按如下格式输出：
 *
 * "a->b" ，如果 a != b
 * "a" ，如果 a == b
 *
 * 输入：nums = [0,1,2,4,5,7]
 * 输出：["0->2","4->5","7"]
 * 解释：区间范围是：
 * [0,2] --> "0->2"
 * [4,5] --> "4->5"
 * [7,7] --> "7"
 *
 * 输入：nums = [0,2,3,4,6,8,9]
 * 输出：["0","2->4","6","8->9"]
 * 解释：区间范围是：
 * [0,0] --> "0"
 * [2,4] --> "2->4"
 * [6,6] --> "6"
 * [8,9] --> "8->9"
 *
 *
 */
public class _228summaryRanges {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.4 MB     * , 在所有 Java 提交中击败了     * 73.57%     * 的用户
     *
     * @param nums
     * @return
     */
    public static List<String> summaryRanges(int[] nums) {
        List<String> list = new ArrayList<>();
        if(nums.length==0)return list;
        if(nums.length==1){
            list.add(String.valueOf(nums[0]));
            return list;
        }
        int start = nums[0];
        int end = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if(nums[i] - nums[i-1] == 1){
                end = nums[i];
            }else {
                StringBuilder temp = new StringBuilder();
                temp.append(start);
                if(end != start){
                    temp.append("->");
                    temp.append(end);
                }
                list.add(temp.toString());
                start = nums[i];
                end = nums[i];
            }
        }
        if(start==end){
            list.add(String.valueOf(start));
        }else {
            StringBuilder temp = new StringBuilder();
            temp.append(start);
            temp.append("->");
            temp.append(end);
            list.add(temp.toString());
        }
        return list;
    }

    public static void main(String[] args) {
        for (String s :
                summaryRanges(new int[]{0,2,3,4,6,8,9})) {
            System.out.println(s);
        }
    }

}
