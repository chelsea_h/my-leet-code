package code701_800.code30_40;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 有一幅以二维整数数组表示的图画，每一个整数表示该图画的像素值大小，数值在 0 到 65535 之间。
 *
 * 给你一个坐标 (sr, sc) 表示图像渲染开始的像素值（行 ，列）和一个新的颜色值 newColor，让你重新上色这幅图像。
 *
 * 为了完成上色工作，从初始坐标开始，记录初始坐标的上下左右四个方向上像素值与初始坐标相同的相连像素点，接着再记录这四个方向上符合条件的像素点与他们对应四个方向上像素值与初始坐标相同的相连像素点，……，重复该过程。将所有有记录的像素点的颜色值改为新的颜色值。
 *
 * 最后返回经过上色渲染后的图像。
 *
 * 输入:
 * image = [[1,1,1],[1,1,0],[1,0,1]]
 * sr = 1, sc = 1, newColor = 2
 * 输出: [[2,2,2],[2,2,0],[2,0,1]]
 * 解析:
 * 在图像的正中间，(坐标(sr,sc)=(1,1)),
 * 在路径上所有符合条件的像素点的颜色都被更改成2。
 * 注意，右下角的像素没有更改为2，
 * 因为它不是在上下左右四个方向上与初始点相连的像素点。
 *
 *
 */
public class _733floodFill {

    /**
     * 是一个图搜索问题，无论是深搜还是广搜
     * 注意：Sr表示行，所以加减为行加减。sc表示列。
     *
     * 执行用时：     * 2 ms     * , 在所有 Java 提交中击败了     * 61.40%     * 的用户
     * 内存消耗：     * 38.6 MB     * , 在所有 Java 提交中击败了     * 98.66%     * 的用户
     * @param image
     * @param sr
     * @param sc
     * @param newColor
     * @return
     */
    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        // 方向向量
        int[] dx = {1,0,0,-1};
        int[] dy = {0,1,-1,0};
        // 如果目标区域为当前颜色，则直接返回原数组
        int curColor = image[sr][sc];
        if(curColor==newColor) return image;
        // m,n记录数组边界，用来判断是否已经到达边界
        int m = image.length;
        int n = image[0].length;
        // 开始入队进行广度优先遍历
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{sr,sc});
        image[sr][sc] = newColor;
        int[] cell;
        while (!queue.isEmpty()){
            cell = queue.poll();
            int x = cell[0];
            int y = cell[1];
            // 遍历四个方向
            for (int i = 0; i < 4; i++) {
                int mx = x+dx[i];
                int my = y+dy[i];
                if(mx>=0&&mx<m&&my>=0&&my<n&&image[mx][my]==curColor){
                    queue.offer(new int[]{mx,my});
                    image[mx][my] = newColor;
                }
            }
        }
        return image;
    }

}
