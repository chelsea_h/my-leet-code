package code701_800.code40_50;

/**
 * 给你一个字符串 licensePlate 和一个字符串数组 words ，请你找出并返回 words 中的 最短补全词 。
 *
 * 补全词 是一个包含 licensePlate 中所有的字母的单词。在所有补全词中，最短的那个就是 最短补全词 。
 *
 * 在匹配 licensePlate 中的字母时：
 * 忽略 licensePlate 中的 数字和空格 。
 * 不区分大小写。
 * 如果某个字母在 licensePlate 中出现不止一次，那么该字母在补全词中的出现次数应当一致或者更多。
 *
 * 输入：licensePlate = "1s3 PSt", words = ["step", "steps", "stripe", "stepple"]
 * 输出："steps"
 * 解释：最短补全词应该包括 "s"、"p"、"s"（忽略大小写） 以及 "t"。
 * "step" 包含 "t"、"p"，但只包含一个 "s"，所以它不符合条件。
 * "steps" 包含 "t"、"p" 和两个 "s"。
 * "stripe" 缺一个 "s"。
 * "stepple" 缺一个 "s"。
 * 因此，"steps" 是唯一一个包含所有字母的单词，也是本例的答案。
 *
 * 示例 2：
 *
 * 输入：licensePlate = "1s3 456", words = ["looks", "pest", "stew", "show"]
 * 输出："pest"
 * 解释：licensePlate 只包含字母 "s" 。所有的单词都包含字母 "s" ，其中 "pest"、"stew"、和 "show" 三者最短。答案是 "pest" ，因为它是三个单词中在 words 里最靠前的那个
 *
 *
 */
public class _748shortestCompletingWord {

    /**
     * 暴力解法
     * 执行用时：     * 5 ms     * , 在所有 Java 提交中击败了     * 37.24%     * 的用户
     * 内存消耗：     * 38.9 MB     * , 在所有 Java 提交中击败了     * 97.65%     * 的用户
     * @param licensePlate
     * @param words
     * @return
     */
    public static String shortestCompletingWord(String licensePlate, String[] words) {
        licensePlate = licensePlate.toUpperCase();
        StringBuffer shortWord = new StringBuffer();
        for (int i = 0; i < licensePlate.length(); i++) {
            if(licensePlate.charAt(i)>=60&&licensePlate.charAt(i)<=90){
                shortWord.append(licensePlate.charAt(i));
            }
        }
        int length = 0;
        int index = Integer.MAX_VALUE;
        for (int i = 0; i < words.length; i++) {
            if(contains(words[i].toUpperCase(),shortWord.toString())){
                if (i<index){
                    index = i;
                    length = words[i].length();
                }
                if(length>words[i].length()){
                    length = words[i].length();
                    index = i;
                }
            }
        }
        return words[index];
    }

    public static boolean contains(String word,String shortWord){
        int result = 0;
        char[] temp = shortWord.toCharArray();
        char[] words = word.toCharArray();
        for (int i = 0; i < temp.length; i++){
            for (int j = 0; j < words.length; j++) {
                if(temp[i]==words[j]){
                    words[j] = ' ';
                    result++;
                    break;
                }
            }
        }
        if(result == temp.length)return true;
        return false;
    }

    public static void main(String[] args) {
        shortestCompletingWord("Ah71752",new String[]{"suggest","letter","of","husband","easy","education","drug","prevent","writer","old"});
    }
}
