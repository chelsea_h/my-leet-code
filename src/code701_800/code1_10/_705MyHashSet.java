package code701_800.code1_10;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * 不使用任何内建的哈希表库设计一个哈希集合（HashSet）。
 *
 * 实现 MyHashSet 类：
 *
 * void add(key) 向哈希集合中插入值 key 。
 * bool contains(key) 返回哈希集合中是否存在这个值 key 。
 * void remove(key) 将给定值 key 从哈希集合中删除。如果哈希集合中没有这个值，什么也不做。
 *
 * 输入：
 * ["MyHashSet", "add", "add", "contains", "contains", "add", "contains", "remove", "contains"]
 * [[], [1], [2], [1], [3], [2], [2], [2], [2]]
 * 输出：
 * [null, null, null, true, false, null, true, null, false]
 *
 * 解释：
 * MyHashSet myHashSet = new MyHashSet();
 * myHashSet.add(1);      // set = [1]
 * myHashSet.add(2);      // set = [1, 2]
 * myHashSet.contains(1); // 返回 True
 * myHashSet.contains(3); // 返回 False ，（未找到）
 * myHashSet.add(2);      // set = [1, 2]
 * myHashSet.contains(2); // 返回 True
 * myHashSet.remove(2);   // set = [1]
 * myHashSet.contains(2); // 返回 False ，（已移除）
 *
 */
public class _705MyHashSet {

    ListNode hashSet;

    /**
     * 执行用时：     * 356 ms     * , 在所有 Java 提交中击败了     * 8.45%     * 的用户
     * 内存消耗：     * 45 MB     * , 在所有 Java 提交中击败了     * 60.57%     * 的用户
     */
    public _705MyHashSet() {
        hashSet = new ListNode(-1);
    }

    // 维护有序数组
    public void add(int key) {
        ListNode temp = hashSet;
        if(temp.val==-1){
            temp.val = key;
            return;
        }
        while (temp!=null){
            if(temp.val==key){
                break;
            }else if(temp.val<key){
                if(temp.next==null){
                    ListNode node = new ListNode(key);
                    temp.next = node;
                    return;
                }
                temp = temp.next;
            }else {
                // 插入节点
                ListNode node = new ListNode(temp.val);
                temp.val = key;
                node.next = temp.next;
                temp.next = node;
                break;
            }
        }
    }

    public void remove(int key) {
        ListNode temp = hashSet.next;
        ListNode pre = hashSet;
        if(pre.val==key){
            pre = pre.next;
            return;
        }
        if(temp==null)return;
        if(temp.val==key&&temp.next==null){
            pre.next = null;
            return;
        }
        while (temp!=null){
            // 删除当前节点
            if(key == temp.val){
                if(temp.next!=null){
                    temp.val = temp.next.val;
                    temp.next = temp.next.next;
                }else {
                    // 删除最后一个节点
                    pre.next = null;
                }
                break;
            }else {
                temp = temp.next;
                pre = pre.next;
            }
        }
    }

    public boolean contains(int key) {
        ListNode temp = hashSet;
        while (temp!=null){
            if(key == temp.val){
                return true;
            }
            temp = temp.next;
        }
        return false;
    }

    public static void main(String[] args) {
        _705MyHashSet myHashSet = new _705MyHashSet();
        myHashSet.add(1);
        myHashSet.add(2);
        System.out.println(myHashSet.contains(1));
        System.out.println(myHashSet.contains(3));
        myHashSet.add(2);
        System.out.println(myHashSet.contains(2));
        myHashSet.remove(2);
        System.out.println(myHashSet.contains(2));
    }

}

/**
 * 题解方法
 *
 * 执行用时： * 20 ms * , 在所有 Java 提交中击败了 * 26.58% * 的用户
 * 内存消耗： * 45.1 MB * , 在所有 Java 提交中击败了 * 57.15% * 的用户
 *
 */
class MyHashSet {
    private static final int BASE = 769;
    private LinkedList[] data;

    /** Initialize your data structure here. */
    public MyHashSet() {
        data = new LinkedList[BASE];
        for (int i = 0; i < BASE; ++i) {
            data[i] = new LinkedList<Integer>();
        }
    }

    public void add(int key) {
        int h = hash(key);
        Iterator<Integer> iterator = data[h].iterator();
        while (iterator.hasNext()) {
            Integer element = iterator.next();
            if (element == key) {
                return;
            }
        }
        data[h].offerLast(key);
    }

    public void remove(int key) {
        int h = hash(key);
        Iterator<Integer> iterator = data[h].iterator();
        while (iterator.hasNext()) {
            Integer element = iterator.next();
            if (element == key) {
                data[h].remove(element);
                return;
            }
        }
    }

    /** Returns true if this set contains the specified element */
    public boolean contains(int key) {
        int h = hash(key);
        Iterator<Integer> iterator = data[h].iterator();
        while (iterator.hasNext()) {
            Integer element = iterator.next();
            if (element == key) {
                return true;
            }
        }
        return false;
    }

    private static int hash(int key) {
        return key % BASE;
    }
}