package code701_800.code1_10;

/**
 * 给你一个字符串 s ，将该字符串中的大写字母转换成相同的小写字母，返回新的字符串。
 *
 * 输入：s = "Hello"
 * 输出："hello"
 *
 * 输入：s = "here"
 * 输出："here"
 */
public class _709toLowerCase {

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.3 MB     * , 在所有 Java 提交中击败了     * 97.41%     * 的用户
     * @param s
     * @return
     */
    public String toLowerCase(String s) {
        return s.toLowerCase();
    }

    /**
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 36.2 MB     * , 在所有 Java 提交中击败了     * 97.03%     * 的用户
     * @param s
     * @return
     */
    public String toLowerCase1(String s) {
        StringBuilder sb = new StringBuilder();
        for (char c : s.toCharArray()) {
            if (c >= 'A' && c <= 'Z') {
                sb.append((char)(c + 32));
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }
}
