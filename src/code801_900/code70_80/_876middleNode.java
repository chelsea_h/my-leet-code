package code801_900.code70_80;

/**
 * 给定一个头结点为 head 的非空单链表，返回链表的中间结点。
 *
 * 如果有两个中间结点，则返回第二个中间结点。
 *
 * 输入：[1,2,3,4,5]
 * 输出：此列表中的结点 3 (序列化形式：[3,4,5])
 * 返回的结点值为 3 。 (测评系统对该结点序列化表述是 [3,4,5])。
 * 注意，我们返回了一个 ListNode 类型的对象 ans，这样：
 * ans.val = 3, ans.next.val = 4, ans.next.next.val = 5, 以及 ans.next.next.next = NULL.
 *
 * 输入：[1,2,3,4,5,6]
 * 输出：此列表中的结点 4 (序列化形式：[4,5,6])
 * 由于该列表有两个中间结点，值分别为 3 和 4，我们返回第二个结点。
 *
 * 来源：力扣（LeetCode）
 */
public class _876middleNode {

    /**
     * 思考 ： 第一下就想到了快慢指针，绝对没问题，也是On复杂度
     *
     * 执行用时：     * 0 ms     * , 在所有 Java 提交中击败了     * 100.00%     * 的用户
     * 内存消耗：     * 35.7 MB     * , 在所有 Java 提交中击败了     * 93.75%     * 的用户
     *
     * @param head
     * @return
     */
    public ListNode middleNode(ListNode head) {
        if(head.next==null) return head;
        ListNode pre = head;
        ListNode last = head;
        while (last!=null){
            if(last.next!=null){
                last = last.next.next;
            }else {
                break;
            }
            pre = pre.next;
        }
        return pre;
    }

}
