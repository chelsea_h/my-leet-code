package code601_700.code10_20;

import java.util.Stack;

/**
 * 给定两个二叉树，想象当你将它们中的一个覆盖到另一个上时，两个二叉树的一些节点便会重叠。
 *
 * 你需要将他们合并为一个新的二叉树。合并的规则是如果两个节点重叠，那么将他们的值相加作为节点合并后的新值，
 * 否则不为 NULL 的节点将直接作为新二叉树的节点。
 *
 */
public class _617mergeTree {

    /**
     * 思考：本质上还是二叉树的遍历
     *
     * 执行用时：     * 2 ms     * , 在所有 Java 提交中击败了     * 19.48%     * 的用户
     * 内存消耗：     * 38.1 MB     * , 在所有 Java 提交中击败了     * 98.23%     * 的用户
     * @param root1
     * @param root2
     * @return
     */
    public TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
        if(root1==null)return root2;
        if(root2==null)return root1;
        Stack<TreeNode> stack1 = new Stack<>();
        Stack<TreeNode> stack2 = new Stack<>();
        Stack<TreeNode> stack = new Stack<>();

        TreeNode node1;
        TreeNode node2;
        TreeNode node;
        TreeNode result = new TreeNode(root1.val+root2.val);

        stack1.push(root1);
        stack2.push(root2);
        stack.push(result);
        while (!stack1.empty()&&!stack2.empty()){
            node1 = stack1.pop();
            node2 = stack2.pop();
            node = stack.pop();
            TreeNode left1 = node1.left,left2 = node2.left,right1 = node1.right,right2 = node2.right;
            if(left1!=null||left2!=null){
                if(left1!=null&&left2!=null){
                    TreeNode left = new TreeNode(left1.val+left2.val);
                    node.left = left;
                    stack.push(left);
                    stack1.push(left1);
                    stack2.push(left2);
                }else if(left1!=null){
                    node.left = left1;
                }else if(left2!=null){
                    node.left = left2;
                }
            }
            if(right1!=null||right2!=null){
                if(right1!=null&&right2!=null){
                    TreeNode right = new TreeNode(right1.val+right2.val);
                    node.right = right;
                    stack.push(right);
                    stack1.push(right1);
                    stack2.push(right2);
                }else if(right1!=null){
                    node.right = right1;
                }else if(right2!=null){
                    node.right = right2;
                }
            }
        }
        return result;
    }
}
