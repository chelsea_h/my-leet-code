package code601_700.code80_90;

/**
 * 给定两个字符串 a 和 b，寻找重复叠加字符串 a 的最小次数，使得字符串 b 成为叠加后的字符串 a 的子串，如果不存在则返回 -1。
 *
 * 注意：字符串 "abc" 重复叠加 0 次是 ""，重复叠加 1 次是 "abc"，重复叠加 2 次是 "abcabc"。
 *
 * 输入：a = "abcd", b = "cdabcdab"
 * 输出：3
 * 解释：a 重复叠加三遍后为 "abcdabcdabcd", 此时 b 是其子串。
 *
 * 输入：a = "a", b = "aa"
 * 输出：2
 *
 * 输入：a = "a", b = "a"
 * 输出：1
 *
 * 输入：a = "abc", b = "wxyz"
 * 输出：-1
 */
public class _686repeatedStringMatch {

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public int repeatedStringMatch(String a, String b) {
        return 0;
    }

}
